ISW Git Tools
=============

The ISW git tools are a collection of scripts and guides to support development
of repositories for different languages. It provides for example guides to setup
the GitLab SSH access or configuration of the SSH agent. Furthermore scripts for
versioning of repositories or createing API documentation and so on. Each
language support is has it's own folder which are located in the `repo-scripts`
directory. Every language support has a `README.md` that describes the features
of the specific language support and how to setup and configure it. Currently
the following languages are supported:
* Fluent Scheme
* Tcl/Tk

If you'd like to contribute, please read the 
[contribution guide](CONTRIBUTING.md) first.

Usage
-----
Before proceeding, make sure that the GitLab access on your user account is 
correctly configured (needs to be done only once per operating system user 
account). If not, read the [Setup Gitlab Access](/guides/setup-gitlab-access.md)
guide to configure the access properly.  

To create a new repository with `isw-git-tools` support, first check if the
(main) language of the new repository is supported (look into the `repo-scripts`
directory). If not, see the [contribution guide](CONTRIBUTING.md) for how to add
the language support. Now create a new project on the gitlab page, preferably in
the [jkuisw](https://gitlab.com/jkuisw) group.

> **Note:** Please stick to the naming convention to prepend the
> project/repository name with a language specific identifier:
> * for Fluent Scheme: `fscm-`
> * for Tcl/Tk: `tcltk-`
>
> This helps to quickly find the repositories for a specific language, see the
> existing ones for examples.

Furthermore choose a license for the repository, this can be done directly on
the GitLab homepage, which will offer you templates for the most common
licenses. After creating the project on GitLab, it should describe how to add a
license. Ignore the other initialization steps described, we will cover that in
the following steps.  
The next steps will be done local on your computer which you want to use for
developing of the new repository. First the newly created repository (project)
needs to be cloned to your computer with the command:

```shell
git clone git@gitlab.com:<group>/<project name>.git
```

The `<group>` is normally `jkuisw` and `<project name>` is the name of your
newly created project. You can find the correct path on GitLab at the project
site of the repository (the SSH path on top of the project's overview page).
Every repository should contain the following two files:
* `README.md` - Description what the repository is, what it does, how to use it
  and probably show some examples and/or descriptions of the most important
  features. If this file has exactly that name, than GitLab will show it by
  default on the overview page of the repository project.
* `CONTRIBUTING.md` - Describes how this repository is developed, defines
  conventions used, explains how the code works, describes concepts used. In
  other words, what a person needs to know if he/she want's to join the
  development of the repository (and had not created it).

These files will grow during the repository development, to create them execute
the commands: 

```shell
touch README.md
touch CONTRIBUTING.md
```

The next step is to add the `isw-git-tools` repository as a git submodule for
the language support. To do this create a directory with the name `submodules`
and execute the corresponding git command as follows:

```shell
# Create the submodules directory.
mkdir submodules

# Add the isw-git-tools as submodule.
git submodule add -b master git@gitlab.com:jkuisw/isw-git-tools.git submodules/isw-git-tools
```

Now is a good time to commit the changes made so far to the repository by the
following commands:

```shell
git add --all
git commit -m "create initial files of repository, add submodule \"isw-git-tools\""
```

After that the language specific setup needs to be done, for this go into the
language support folder (in the `repo-scripts` folder) and read the `README.md`
of the language support to set it up (e.g. for the fluent scheme language
support read `/repo-scripts/fluent-scheme/README.md`).  
When finished the language setup, the last step is to push the changes to the
remote repository (which is GitLab):

```shell
git push
```

Now you can start developing the new repository.

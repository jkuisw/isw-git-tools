#!/bin/bash

#*******************************************************************************
# global variables
#*******************************************************************************
scriptDir=$(cd "$(dirname $0)" && pwd)
scriptDir=$(readlink -f "$scriptDir")
repoDir=$(readlink -f "$scriptDir/../../../../..")
genDocVersionRegex=".*\\[\\[create[[:space:]]+version[[:space:]]+(\"|)"
genDocVersionRegex+="([^] \"]+)([[:space:]]*|\"[[:space:]]*)\\]\\].*"
skipCi="no"
tagPassed="no"
tag=""

#*******************************************************************************
# load dependencies
#*******************************************************************************
# shellcheck source=../../../scripts/semver/semver.sh
source "$scriptDir/../../../scripts/semver/semver.sh"

#*******************************************************************************
# main function
#*******************************************************************************
function main {
  local commitMessage=""
  local version=""
  local commitMessage=""
  
  # Parse command line parameters.
  parseParameters "$@"
  
  # Check if repository directory is a git repository.
  git rev-parse || exit $?
  
  # Check if in commit message mode or tag mode.
  if [[ $tagPassed = "yes" ]]; then
    # In tag mode.
    # Delete existing tag, because it will be created again after generating API
    # documentation, so that changes are included in that tag.
    git push --delete origin $tag || exit $?
    git tag --delete $tag || exit $?
    version=$tag
  else
    # In commit message mode.
    # Get and check last commit message.
    commitMessage=$(git log -1 --pretty=%B) || exit $?
    if [[ "$commitMessage" =~ $genDocVersionRegex ]]; then
      # Get new version from commit messsage.
      version=${BASH_REMATCH[2]}
    else 
      echo "#####################################################################"
      echo "No command in last commit message for packaging fluent scheme module."
      echo "Last commit message: \"$commitMessage\""
      echo "#####################################################################"
      exit 0
    fi
  fi
  
  # Start packaging of fluent scheme module.
  echo "#####################################################################"
  echo "Packaging fluent scheme module."
  echo "#####################################################################"
  echo "Settings"
  echo "  -> Repository Directory: $repoDir"
  echo "  -> Skip CI: $skipCi"
  echo "  -> Version: $version"
  
  # Generate API documentation and if successful commit it.
  "$repoDir"/scripts/generateDoc.sh || exit $?
  git add "$repoDir"/doc/* || exit $?
  commitMessage="Generated API documentation for version \"$version\"."
  if [[ $skipCi = "yes" ]]; then
    # Commit should be successful even if there were no changes.
    git commit -m "[skip ci] $commitMessage" || true 
  else
    # Commit should be successful even if there were no changes.
    git commit -m "$commitMessage" || true
  fi
  
  # Create the new module version.
  "$repoDir"/scripts/exCreateVersion.sh "$version" "$skipCi" || exit $?
}

#*******************************************************************************
# parse parameters
#*******************************************************************************
function parseParameters {
  #-----------------------------------------------------------------------------
  # Parse option arguments.
  while getopts ":hst:r:" opt; do
    case $opt in
      h)
        printHelp
        exit 0
        ;;
      s)
        skipCi="yes"
        ;;
      t)
        tag="$OPTARG"
        tagPassed="yes"
        ;;
      r)
        # Check if repository directory exists.
        if [[ ! -d $OPTARG ]]; then
          echo "The repository directory \"$OPTARG\" does not exist."
          printUsage
          exit 1
        fi
        repoDir=$(readlink -f "$OPTARG")
        repoDir=${repoDir%/} # remove trailing slash if existing
        ;;
      \?)
        echo "Invalid option: -$OPTARG"
        printUsage
        exit 1
        ;;
      :)
        echo "Option -$OPTARG requires an argument."
        printUsage
        exit 1
        ;;
    esac
    
    if [[ $tagPassed = "yes" ]]; then
      # Git tag was passed as parameter, therefore check if it is a valid
      # version according to the semantic versioning specification: 
      # http://semver.org/
      semverIsValid $tag
      if [[ $? -eq 1 ]]; then
        echo "The passed tag \"$tag\" is not a valid version according to the"
        echo "semantic versioning specification: http://semver.org/"
        echo "Packaging of fluent scheme module skipped."
        exit 0
      fi
    fi
  done
}

#*******************************************************************************
# Prints the usage to sdtout.
#*******************************************************************************
function printUsage {
  echo "USAGE"
  echo "    packageFscmModule.sh [-h] [-s] [-t <tag>] [-r <repo-path>]"
  echo ""
}

#*******************************************************************************
# Prints the help to stdout.
#*******************************************************************************
function printHelp {
  echo ""

  echo "NAME"
  echo "    packageFscmModule.sh - Generate API doc and create new module"
  echo "      version when requested by last commit message or a given tag."
  echo ""

  printUsage

  echo "DESCRIPTION"
  echo "    Two modes are supported by the script:"
  echo "    First when the -t (tag) parameter is NOT passed, than the script"
  echo "    checks the last git commit message if it contains the following"
  echo "    command: [[create version <semver string>]]"
  echo "    Example for a commit message with the command: "
  echo "      \"any arbitrary text [[create version 1.2.3]] any arbitrary text\""
  echo "      => new version to create: \"1.2.3\""
  echo "    When the command is found in the last commit message, than the API"
  echo "    documentation for the fluent scheme module will be generated for"
  echo "    the new version. When successful, than the new module version will"
  echo "    be created."
  echo "    Second when the -t (tag) parameter IS passed, then the tag must be"
  echo "    a valid semantic versioning version. If so, the API documentation"
  echo "    for the fluent scheme module will be generated for the new version."
  echo "    When successful, than the new module version will be created. The"
  echo "    commit message will be ignored in this mode."
  echo ""

  echo "OPTIONS"
  echo "    -h"
  echo "      Display this help and exit."
  echo ""
  echo "    -s"
  echo "      To every git commit message the string \"[skip ci]\" will be"
  echo "      prepended if the flag is passed. This is needed when the skript"
  echo "      runs in an gitlab ci job to avoid looping ci jobs."
  echo ""
  echo "    -t <tag>"
  echo "      When passed, then the script works in the second mode. The (git)"
  echo "      tag passed must be a valid semantic versioning version, see:"
  echo "      http://semver.org/ for further information. If not, then the"
  echo "      script will return with no error and do nothing."
  echo ""
  echo "    -r <repo-path>"
  echo "      The path to the directory of the module repository. Should be an"
  echo "      absolute path or realtive from the location of the current working"
  echo "      directory. Default is five directories above the directory of this"
  echo "      script."
  echo ""
}

#*******************************************************************************
# Call main function and pass parameters.
#*******************************************************************************
main "$@"

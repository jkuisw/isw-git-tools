#!/bin/bash

#*******************************************************************************
# global variables
#*******************************************************************************
scriptDir=$(cd "$(dirname $0)" && pwd)
scriptDir=$(readlink -f "$scriptDir")
repoDir=$(readlink -f "$scriptDir/../../../../..")
docDir="$repoDir/doc"
srcDir="$repoDir/src"
tmpDir=""
procsGloballyExported="no"
deleteTempFiles="yes"
descriptionFileName="description"
shortDescrLength=76

#*******************************************************************************
# load dependencies
#*******************************************************************************
# shellcheck source=docTokenPatterns.sh
source "$scriptDir/docTokenPatterns.sh"
# shellcheck source=fscmDocParser.sh
source "$scriptDir/fscmDocParser.sh"
# shellcheck source=fscmDocBuilder.sh
source "$scriptDir/fscmDocBuilder.sh"

#*******************************************************************************
# main function
#*******************************************************************************
function main {
  echo "#######################################################################"
  echo "Generating source code documentation."
  echo "#######################################################################"
  # Parse command line parameters.
  parseParameters "$@"

  # Delete documentation directory if existing and create it afterwards.
  if [[ -d "$docDir" ]]; then
    rm -rfd "$docDir" || exit $?
  fi
  mkdir -p "$docDir" || exit $?

  # Create tmp directory.
  mkdir -p "$tmpDir" || exit $?

  # Find all source files and parse them.
  echo "Settings"
  echo "  -> Repository directory: $repoDir"
  echo "  -> Documentation directory: $docDir"
  echo "  -> Source directory: $srcDir"
  echo "  -> Globally exported procedures: $procsGloballyExported"
  echo "  -> Delete temporary generated files: $deleteTempFiles"
  echo "Parsing all source files in \"$srcDir\""
  shopt -s globstar # Set (enable) globstar shell option.
  for file in "$srcDir"/**/*.scm; do
    parseSourceFile $file $tmpDir $descriptionFileName $repoDir || exit $?
  done
  shopt -u globstar # Unset (disable) globstar shell option.

  # Generate the documentation.
  echo "Generating documentation files."
  generateOverviewDoc $srcDir $docDir $tmpDir $descriptionFileName $repoDir \
    $shortDescrLength || exit $?
  generateFileDoc $docDir $tmpDir $descriptionFileName $repoDir \
    $shortDescrLength $procsGloballyExported || exit $?
  
  # Delete temporary generated files.
  if [[ $deleteTempFiles = "yes" ]]; then
    rm -rfd "$tmpDir" || exit $?
  fi
  
  echo "Successfully created the module documentation."
}

#*******************************************************************************
# parse parameters
#*******************************************************************************
function parseParameters {
  #-----------------------------------------------------------------------------
  # Parse option arguments.
  while getopts ":hr:d:s:gt" opt; do
    case $opt in
      h)
        printHelp
        exit 0
        ;;
      r)
        # Check if repository directory exists.
        if [[ ! -d $OPTARG ]]; then
          echo "The repository directory \"$OPTARG\" does not exist."
          printUsage
          exit 1
        fi
        repoDir=$(readlink -f "$OPTARG")
        repoDir=${repoDir%/} # remove trailing slash if existing
        ;;
      d)
        docDir=$(readlink -f "$OPTARG")
        docDir=${docDir%/} # remove trailing slash if existing
        ;;
      s)
        srcDir=$(readlink -f "$OPTARG")
        srcDir=${srcDir%/} # remove trailing slash if existing
        ;;
      g)
        procsGloballyExported="yes"
        ;;
      t)
        deleteTempFiles="no"
        ;;
      \?)
        echo "Invalid option: -$OPTARG"
        printUsage
        exit 1
        ;;
      :)
        echo "Option -$OPTARG requires an argument."
        printUsage
        exit 1
        ;;
    esac
  done

  tmpDir="$docDir/tmp"
}

#*******************************************************************************
# Prints the usage to sdtout.
#*******************************************************************************
function printUsage {
  echo "USAGE"
  echo "    fscmDocGenerator.sh [-h] [-r <repo-path>] [-d <doc-path>] [-s <source-path>]"
  echo ""
}

#*******************************************************************************
# Prints the help to stdout.
#*******************************************************************************
function printHelp {
  echo ""

  echo "NAME"
  echo "    fscmDocGenerator.sh - Generate documentation for a fluent scheme module."
  echo ""

  printUsage

  echo "DESCRIPTION"
  echo "    This script generates the API documentation for a fluent scheme "
  echo "    module."
  echo ""

  echo "OPTIONS"
  echo "    -h"
  echo "      Display this help and exit."
  echo ""
  echo "    -r <repo-path>"
  echo "      The path to the directory of the module repository. Should be an"
  echo "      absolute path or realtive from the location of the current working"
  echo "      directory. Default is five directories above the directory of this"
  echo "      script."
  echo ""
  echo "    -d <doc-path>"
  echo "      The path to the directory which contains the generated "
  echo "      documentation, relative from the repository directory."
  echo "      Default is: \"doc\""
  echo ""
  echo "    -s <source-path>"
  echo "      The path to the directory which contains the source code, relative"
  echo "      from the repository directory. The script will search for all"
  echo "      files matching the globbing pattern \"*.scm\" in this directory"
  echo "      and all it's subdirectories. Default is: \"src\""
  echo ""
  echo "    -g"
  echo "      If this flag is set, than the exported procedures are exported"
  echo "      globally. This means they are not imported with the 'import'"
  echo "      command and therefore will not be prepended with the import name."
  echo "      Normally this is only used for the module manager, which"
  echo "      provides for example the 'import' procedure."
  echo ""
  echo "    -t"
  echo "      If this flag is passed, than the temporary generated files will"
  echo "      not be deleted."
  echo ""
}

#*******************************************************************************
# Call main function and pass parameters.
#*******************************************************************************
main "$@"

#*******************************************************************************
# TODO
#*******************************************************************************

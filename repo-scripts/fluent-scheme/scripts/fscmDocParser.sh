#!/bin/bash

#*******************************************************************************
# global variables
#*******************************************************************************

#*******************************************************************************
# load dependencies
#*******************************************************************************

#*******************************************************************************
# Parse the API documentation from the given source file.
#
# parameters:
#   $1 - [required] The file (path) to parse.
#   $2 - [required] The path to a directory for creating temporary files. Used
#        for the intermediate format.
#   $3 - [required] The name of the file in the intermediate format where the
#        attributes for a source file are saved, normally "description".
#   $4 - [required] The path to the repository directory.
#
# returns:
#   0 on success, not 0 otherwise.
#*******************************************************************************
function parseSourceFile {
  local state="searchBlockStart"
  local docBlock=""
  local procHeader=""
  local exportBlock=""
  local blockCount=0
  local lineCount=0
  local lineNumberOfProcedure=0
  local docBlockLineCount=0
  # shellcheck disable=SC2034
  local maxParamId=-1 # used by indirection
  local procCount=0
  local file=${1:?}
  local tmpDir=${2:?}
  local descriptionFileName=${3:?}
  local repoDir=${4:?}
  local fileDirectory
  local descriptionFile
  declare -A fileData
  declare -A procNameIdMap
  declare -A procNameExportNameMap

  # Create directory for parsed data of the source file.
  fileDirectory="$tmpDir"/$(basename "$file") || return $?
  fileDirectory=${fileDirectory%%.scm} # strip file extension
  mkdir -p $fileDirectory

  # Create procedures directory.
  mkdir -p "$fileDirectory/procs"

  # Create parameters directory.
  mkdir -p "$fileDirectory/params"

  # Set the description file path.
  descriptionFile="$fileDirectory/$descriptionFileName"

  # Source file parameters.
  fileData[name]=$(basename "$file") || return $?
  fileData[linkPath]=${file##"$repoDir"}
  fileData[hasExports]="no"

  # Parsing source file.
  echo "  parsing \"$file\""

  # Loop through lines of file.
  while IFS='' read -r line || [[ -n "$line" ]]; do
    _=$((lineCount++))
    line=${line//$'\r'/} # Convert to linux line endings if not ('\r\n' -> '\n')
    case "$state" in
      "searchBlockEnd")
        _=$((docBlockLineCount++))

        # Check if block should be ignored.
        if [[ $docBlockLineCount -eq 1 ]] && [[ $line =~ ${tokenIgnoreDocBlock:?} ]]; then
          state="searchBlockStart" # Ignore documentation block.
        else
          if [[ $line =~ ${tokenDocLine:?} ]]; then
            if [[ -z $docBlock ]]; then
              docBlock=${BASH_REMATCH[1]}
            else
              docBlock=$docBlock$'\n'${BASH_REMATCH[1]}
            fi
          else
            if [[ $line =~ ${tokenProcedureHeaderStart:?} ]]; then
              # Is beginning of procedure header.
              procHeader=$line
              lineNumberOfProcedure=$lineCount
              if [[ $procHeader =~ ${tokenProcedureHeader:?} ]]; then
                # Parse and save procedure data.
                procNameIdMap["${BASH_REMATCH[2]}"]=$procCount

                if [[ -z ${BASH_REMATCH[3]} ]]; then
                  processProcedure "$docBlock" ${BASH_REMATCH[2]} "" \
                    $procCount maxParamId "$fileDirectory" \
                    $lineNumberOfProcedure
                else
                  processProcedure "$docBlock" ${BASH_REMATCH[2]} \
                    "${BASH_REMATCH[4]}" $procCount maxParamId \
                    "$fileDirectory" $lineNumberOfProcedure
                fi

                _=$((procCount++))
                state="searchBlockStart"
              else
                state="parseProcedureHeader"
              fi
            elif [[ $line =~ ${tokenExportBlockStart:?} ]]; then
              # Is beginning of export block.
              exportBlock=$line
              if [[ $exportBlock =~ ${tokenExportBlock:?} ]]; then
                # Parse export definitions and add them to the procedure name -
                # export name map.
                if [[ $exportBlock =~ ${tokenExportDefinitions:?} ]]; then
                  # Expand export definitions to array.
                  IFS='(' read -a expDefs <<< "${BASH_REMATCH[1]}"

                  # Loop through export definitions and parse them.
                  for ((i=0; i<${#expDefs[@]}; i++)); do
                    # Extract export name and procedure name from export definition.
                    if [[ ${expDefs[$i]} =~ ${tokenExportNameProcName:?} ]]; then
                      local expName

                      # Depending on whether export name is a string or symbol it is
                      # in the third or second matching group.
                      if [[ -z ${BASH_REMATCH[2]} ]]; then
                        expName=${BASH_REMATCH[3]}
                      else
                        expName=${BASH_REMATCH[2]}
                      fi

                      # Add entry to the procedure name - export name map.
                      procNameExportNameMap[${BASH_REMATCH[4]}]=$expName
                    fi
                  done
                fi
                state="searchBlockStart"
              else
                state="parseExportBlock"
              fi
            elif [[ -z $line ]] || [[ $line =~ ${tokenFileDescriptionBlockEnd:?} ]]; then
              # Is empty or only whitespace line. Check if first documentation
              # block, because the file description block must be the first
              # documentation block if existing.
              if [[ $blockCount -eq 1 ]]; then
                # Is valid file description block.
                # Remove indent spaces.
                local indentSpaces=""
                if [[ $docBlock =~ ${tokenLeadingSpaces:?} ]]; then
                  indentSpaces="${BASH_REMATCH[1]}"
                fi
                
                if [[ -n $indentSpaces ]]; then
                  docBlock=${docBlock//$'\n'$indentSpaces/$'\n'}
                fi
                
                docBlock=${docBlock//\*/\\*} # Escape * to prevent file globbing.
                docBlock=$(stringSanitizer "$docBlock")  || return $?
                fileData[description]=\"$docBlock\"

                # Go to state: searchBlockStart.
                state="searchBlockStart"
              else
                # End of bock, but no export, procedure or description block.
                # Therefore ignore it and read next one.
                # Go to state: searchBlockStart.
                state="searchBlockStart"
              fi
            elif [[ $line =~ ${tokenBeginDocBlock:?} ]]; then
              # New documentation block start, therefore ignore former.
              docBlock=""
              docBlockLineCount=0
              _=$((blockCount++))
            fi
          fi
        fi
        ;;
      "searchBlockStart")
        if [[ $line =~ ${tokenBeginDocBlock:?} ]]; then
          state="searchBlockEnd"
          docBlock=""
          docBlockLineCount=0
          _=$((blockCount++))
        fi
        ;;
      "parseProcedureHeader")
        procHeader="$procHeader $line"

        if [[ $procHeader =~ ${tokenNotAProcedureHeader:?} ]]; then
          # Not a procedure header.
          echo "[WARNING] Not a procedure header: $procHeader"
          state="searchBlockStart"
        else
          if [[ $line =~ ${tokenProcedureHeaderEnd:?} ]]; then
            #echo "procHeader: $procHeader"
            # Found end of procedure header.
            state="searchBlockStart"

            if [[ $procHeader =~ ${tokenProcedureHeader:?} ]]; then
              # Parse and save procedure data.
              procNameIdMap["${BASH_REMATCH[2]}"]=$procCount

              if [[ -z ${BASH_REMATCH[3]} ]]; then
                processProcedure "$docBlock" ${BASH_REMATCH[2]} "" $procCount \
                  maxParamId "$fileDirectory" $lineNumberOfProcedure
              else
                processProcedure "$docBlock" ${BASH_REMATCH[2]} \
                  "${BASH_REMATCH[4]}" $procCount maxParamId "$fileDirectory" \
                  $lineNumberOfProcedure
              fi

              _=$((procCount++))
            else
              # Invalid procedure header.
              echo "[ERROR] Failed to parse procedure header: $procHeader"
              return 1
            fi
          fi
        fi
        ;;
      "parseExportBlock")
        exportBlock="$exportBlock $line"
        if [[ $line =~ ${tokenExportBlockEnd:?} ]]; then
          # Found end of export block.
          state="searchBlockStart"
          if [[ $exportBlock =~ ${tokenExportBlock:?} ]]; then
            # Parse export definitions and add them to the procedure name -
            # export name map.
            if [[ $exportBlock =~ ${tokenExportDefinitions:?} ]]; then
              # Expand export definitions to array.
              IFS='(' read -a expDefs <<< "${BASH_REMATCH[1]}"

              # Loop through export definitions and parse them.
              for ((i=0; i<${#expDefs[@]}; i++)); do
                # Extract export name and procedure name from export definition.
                if [[ ${expDefs[$i]} =~ ${tokenExportNameProcName:?} ]]; then
                  local expName

                  # Depending on whether export name is a string or symbol it is
                  # in the third or second matching group.
                  if [[ -z ${BASH_REMATCH[2]} ]]; then
                    expName=${BASH_REMATCH[3]}
                  else
                    expName=${BASH_REMATCH[2]}
                  fi

                  # Add entry to the procedure name - export name map.
                  procNameExportNameMap[${BASH_REMATCH[4]}]=$expName
                fi
              done
            fi
          else
            # Invalid export block.
            echo "[ERROR] Failed to parse export block: $exportBlock"
            return 1
          fi
        fi
        ;;
    esac
  done < $file

  # Add export information to exported procedures data.
  for pName in "${!procNameExportNameMap[@]}"; do
    local procId=${procNameIdMap[$pName]}
    local procFile="$fileDirectory/procs/${procId}.proc"
    local expName=${procNameExportNameMap[$pName]}

    # Check if procedure exists.
    if [[ -z $procId ]]; then
      echo "[ERROR] Invalid export definition, could not find an" \
        "implementation for the procedure \"$pName\"!"
      return 1
    fi

    echo "[isExported]=yes" >> $procFile
    echo "[exportName]=\"${expName//\\/\\\\}\"" >> $procFile
  done
  
  # Write description file.
  if [[ ${#procNameExportNameMap[@]} -gt 0 ]]; then
    fileData[hasExports]="yes"
  fi

  for k in "${!fileData[@]}"; do
    echo "[$k]=${fileData[$k]}" >> $descriptionFile
  done
}

#*******************************************************************************
# Parse the given procedure documentation block and save the parsed information
# to the intermediate format.
#
# parameters:
#   $1 - [required] The documentation block to parse.
#   $2 - [required] The procedure name to which this documentation belongs.
#   $3 - [required] The parameters string for this procedure from the procedure
#        header.
#   $4 - [required] The procedure id for the procedures table in the
#        intermediate format.
#   $5 - [required] The name of the variable where the current maximum id of the
#        parameters table (intermediate format) is saved. The value of this
#        variable will be changed by this procedure (if the procedure to parse
#        has parameters).
#   $6 - [required] The path to the directory where the indermediate format data
#        is saved for the currently parsed source file.
#   $7 - [required] The line number of the currently parsed procedure in the
#        source file of where this procedure is implemented.
#*******************************************************************************
function processProcedure {
  local docBlock=${1:?}
  local procName=${2:?}
  local paramsString=${3?}
  local procId=${4:?}
  local maxParamIdVarName=${5:?}
  local oldMaxParamId=${!maxParamIdVarName}
  local fileDirectory=${6:?}
  local lineNumberOfProcedure=${7:?}
  local state="parseDescription"
  local description=""
  local parameterIds=""
  local procParamCount=0
  local paramId=$oldMaxParamId
  local paramName=""
  local paramDescr=""
  local returnsDescr=""
  local procedureFile="$fileDirectory/procs/${procId}.proc"
  local hasOptionalParams="no"
  local mandatoryParamsCount=0
  local indentSpaces=""
  local isFirstLine="yes"
  local lineCount=0

  unset IFS
  local paramArray=($paramsString)
  
  # Check if the procedure has optional parameters.
  if [[ $paramsString =~ ${tokenParamHasOptional:?} ]]; then
    hasOptionalParams="yes"
  fi

  # Get the number of mandatory parameters of the procedure.
  if [[ $hasOptionalParams = "yes" ]]; then
    # Delete last to elements: '. args'
    unset 'paramArray[${#paramArray[@]} - 1]'
    unset 'paramArray[${#paramArray[@]} - 1]'
  fi
  mandatoryParamsCount=${#paramArray[@]}

  # Loop through lines of documentation block, which is the documentation for
  # the procedure.
  while IFS='' read -r line; do
    case "$state" in
      "parseDescription")
        if [[ $line =~ ${tokenParamBlockStart:?} ]]; then
          indentSpaces=""
          lineCount=0
          paramName=""
          paramDescr=""
          state="parseParameters"
        elif [[ $line =~ ${tokenReturnsBlockStart:?} ]]; then
          indentSpaces=""
          lineCount=1
          state="parseReturns"
          returnsDescr=${BASH_REMATCH[1]}
        else
          if [[ $isFirstLine = "yes" ]]; then
            isFirstLine="no"
            if [[ $line =~ ${tokenLeadingSpaces:?} ]]; then
              indentSpaces="${BASH_REMATCH[1]}"
            fi
          fi
          description=$description$'\n'${line#$indentSpaces}
        fi
        ;;
      "parseParameters")
        if [[ $line =~ ${tokenReturnsBlockStart:?} ]]; then
          indentSpaces=""
          lineCount=1
          state="parseReturns"
          returnsDescr=${BASH_REMATCH[1]}

          if ! [[ -z $paramName ]]; then
            _=$((procParamCount++))
            paramId=$((oldMaxParamId + procParamCount))
            processParameter $procName $paramName \
              "$fileDirectory/params/${paramId}.param" "$paramDescr" \
              $hasOptionalParams ${paramArray[$((procParamCount - 1))]}

            if [[ -z $parameterIds ]]; then
              parameterIds="$paramId"
            else
              parameterIds="$parameterIds $paramId"
            fi

            paramName=""
            paramDescr=""
          fi
        elif [[ $line =~ ${tokenParameterBegin:?} ]]; then
          local tmpParamName=${BASH_REMATCH[1]}
          local tmpParamDescr=${BASH_REMATCH[2]}
          lineCount=1
          
          if ! [[ -z $paramName ]]; then
            _=$((procParamCount++))
            paramId=$((oldMaxParamId + procParamCount))
            processParameter $procName $paramName \
              "$fileDirectory/params/${paramId}.param" "$paramDescr" \
              $hasOptionalParams ${paramArray[$((procParamCount - 1))]}

            if [[ -z $parameterIds ]]; then
              parameterIds="$paramId"
            else
              parameterIds="$parameterIds $paramId"
            fi

            paramName=""
            paramDescr=""
          fi

          paramName=$tmpParamName
          paramDescr=$tmpParamDescr
        else
          _=$((lineCount++))
          if [[ $lineCount -eq 2 ]]; then
            if [[ $line =~ ${tokenLeadingSpaces:?} ]]; then
              indentSpaces="${BASH_REMATCH[1]}"
            fi
          fi
          paramDescr=$paramDescr$'\n'${line#$indentSpaces}
        fi
        ;;
      "parseReturns")
        _=$((lineCount++))
        if [[ $lineCount -eq 2 ]]; then
          if [[ $line =~ ${tokenLeadingSpaces:?} ]]; then
            indentSpaces="${BASH_REMATCH[1]}"
          fi
        fi
        returnsDescr=$returnsDescr$'\n'${line#$indentSpaces}
        ;;
    esac
  done <<< $docBlock
  
  # Check if any parameters is left to process.
  if [[ $state = "parseParameters" ]] && ! [[ -z $paramName ]]; then
    _=$((procParamCount++))
    paramId=$((oldMaxParamId + procParamCount))
    processParameter $procName $paramName \
      "$fileDirectory/params/${paramId}.param" "$paramDescr" \
      $hasOptionalParams ${paramArray[$((procParamCount - 1))]}

    if [[ -z $parameterIds ]]; then
      parameterIds="$paramId"
    else
      parameterIds="$parameterIds $paramId"
    fi

    paramName=""
    paramDescr=""
  fi

  # Check if all (mandatory and at least one optional) parameters are
  # documented.
  if [[ $hasOptionalParams = "yes" ]]; then
    if [[ $procParamCount -le $mandatoryParamsCount ]]; then
      echo "[WARNING] Procedure \"$procName\": There are undocumented parameters!"
    fi
  else
    if [[ $procParamCount -lt $mandatoryParamsCount ]]; then
      echo "[WARNING] Procedure \"$procName\": There are undocumented parameters!"
    fi
  fi

  # Save parsed data to procedure file.
  echo "[name]=\"${procName//\\/\\\\}\"" >> $procedureFile
  description=${description//\*/\\*} # Escape * to prevent file globbing.
  description=$(stringSanitizer "$description") || return $?
  echo "[description]=\"$description\"" >> $procedureFile
  echo "[lineNumber]=$lineNumberOfProcedure" >> $procedureFile
  echo "[parameterIds]=\"$parameterIds\"" >> $procedureFile
  returnsDescr=${returnsDescr//\*/\\*} # Escape * to prevent file globbing.
  returnsDescr=$(stringSanitizer "$returnsDescr") || return $?
  echo "[returns]=\"$returnsDescr\"" >> $procedureFile

  # Update the maximum parameter id variable.
  eval $maxParamIdVarName=$paramId
}

#*******************************************************************************
# Escapes some characters in string to avoid parsing errors.
#
# parameters:
#   $1 - [default: ""] The string to sanitize (optional).
#
# returns:
#   The sanitized string.
#*******************************************************************************
function stringSanitizer {
  local string=${1-""}
  
  # remove leading whitespace characters
  string="${string#"${string%%[![:space:]]*}"}"
  # remove trailing whitespace characters
  string="${string%"${string##*[![:space:]]}"}"

  string=${string//$'\n'/\\n}
  string=${string//\\/\\\\}
  string=${string//\$/\\\$}
  string=${string//\`/\\\`}
  string=${string//\'/\\\'}
  string=${string//\"/\\\"}

  # Return the sanitized string.
  echo "$string"
}

#*******************************************************************************
# Parse the given parameter string and save the parsed information to the
# intermediate format.
#
# parameters:
#   $1 - [required] The name of the procedure the parameter string to parse
#        belongs to. Needed for error and warning messages.
#   $2 - [required] The name of the parameter.
#   $3 - [required] The path to the file where the intermediate data for the
#        parameter to parse should be saved (entry in parameters table of
#        intermediate format).
#   $4 - [required] The parameter string to parse.
#   $5 - [required] Only the values "yes" or "no" are accepted by this
#        parameter. If "yes" is passed, then the procedure the parameter to
#        parse belongs to, has an optional parameter (needed for consistency
#        checks).
#   $6 - [optional, default: ""] The expected name for the parameter to parse.
#        This should be the name that was parsed from the procedure header, so
#        that this function can check that the parameters are documented in the
#        right order. Optional parameters are not named in the procedure header,
#        therefore this parameter is optional.
#*******************************************************************************
function processParameter {
  local procName=${1:?}
  local paramName=${2:?}
  local paramFile=${3:?}
  local paramString=${4:?}
  local procHasOptionalParams=${5:?}
  local expectedParamName=${6-""}
  local description=""
  local isOptional="no"
  local hasDefault="no"
  local defaultValue=""

  # Check if expected parameter name is equal to the documented parameter name.
  # This ensures that the mandatory parameters will be documented in the same
  # order as defined in the procedure header. Because optional parameter names
  # are not listed in the procedure header, this check is not applicable for
  # them.
  if ! [[ -z $expectedParamName ]]; then
    if ! [[ $expectedParamName == "$paramName" ]]; then
      echo "[ERROR] Procedure \"$procName\": Parameters are not documented in" \
        "the same order as defined in the procedure header!"
      return 1
    fi
  fi

  # Check if parameter description has attributes and if so, parse them.
  if [[ $paramString =~ ${tokenParamAttributes:?} ]]; then
    # Parameter description has attributes.
    # Save parameter description.
    description=${BASH_REMATCH[2]}

    # Parse parameter description attributes.
    IFS=$'\n' attributes=(${BASH_REMATCH[1]//, /$'\n'})
    for ((i=0; i<${#attributes[@]}; i++)); do
      if [[ ${attributes[$i]} =~ ${tokenParamAttrIsOptional:?} ]]; then
        isOptional="yes"
      elif [[ ${attributes[$i]} =~ ${tokenParamAttrDefault:?} ]]; then
        hasDefault="yes"
        defaultValue=${BASH_REMATCH[1]}
        # remove leading whitespace characters
        defaultValue="${defaultValue#"${defaultValue%%[![:space:]]*}"}"
        # remove trailing whitespace characters
        defaultValue="${defaultValue%"${defaultValue##*[![:space:]]}"}"
      fi
    done
  else
    # Parameter description does not have attributes.
    description=$paramString
  fi

  # Check that parameter description is not empty.
  if [[ -z $description ]]; then
    echo "[WARNING] Procedure \"$procName\": Missing description for" \
      "parameter \"$paramName\"!"
  fi

  # Check for optional and default value attributes.
  if [[ $procHasOptionalParams = "yes" ]] && [[ -z $expectedParamName ]]; then
    if [[ $isOptional = "yes" ]]; then
      if [[ $hasDefault = "no" ]]; then
        echo "[WARNING] Procedure \"$procName\": Missing default value for" \
          "parameter \"$paramName\"! Because the parameter is optional, a" \
          "default value should be provided."
      fi
    else
      echo "[ERROR] Procedure \"$procName\": Parameter \"$paramName\" is" \
        "optional, but the \"optional\" attribute is missing!"
      return 1
    fi
  fi

  # Save parsed parameter data to the parameter file.
  echo "[name]=\"${paramName//\\/\\\\}\"" >> $paramFile
  description=${description//\*/\\*} # Escape * to prevent file globbing.
  description=$(stringSanitizer "$description") || return $?
  echo "[description]=\"$description\"" >> $paramFile
  echo "[isOptional]=$isOptional" >> $paramFile
  echo "[hasDefault]=$hasDefault" >> $paramFile
  if [[ $hasDefault = "yes" ]]; then
    defaultValue=${defaultValue//\*/\\*} # Escape * to prevent file globbing.
    defaultValue=$(stringSanitizer "$defaultValue") || return $?
    echo "[defaultValue]=\"$defaultValue\"" >> $paramFile
  fi
}

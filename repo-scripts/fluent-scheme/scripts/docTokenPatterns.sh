#!/bin/bash

#*******************************************************************************
# global variables
#*******************************************************************************
# Marks the beginning of a documentation block.
# shellcheck disable=SC2034
tokenBeginDocBlock="^;\\*\\*\\*\\*.*"
# Marks a documentation block to ignore (TODO blocks).
# shellcheck disable=SC2034
tokenIgnoreDocBlock="^;[ ]*TODO.*"
# Marks a documentation line.
# shellcheck disable=SC2034
tokenDocLine="^;(.*)$"

# Makrs the number of leading spaces.
# ^([ ]*).*$
# shellcheck disable=SC2034
tokenLeadingSpaces="^([ ]*).*$"
# Marks the end of a file description block.
# shellcheck disable=SC2034
tokenFileDescriptionBlockEnd="^[[:space:]]*$"

# Marks the beginning of a procedure header.
# shellcheck disable=SC2034
tokenProcedureHeaderStart="^\\(define([ ]+|\\n)"
# Marks the end of a procedure header.
# shellcheck disable=SC2034
tokenProcedureHeaderEnd="[ ]*\\)"
# Marks a procedure header (in one line).
# ^\(define[ ]+\((([^ \)]+)( |)){1}([^\)]+)*\)
# shellcheck disable=SC2034
tokenProcedureHeader="^\\(define[ ]+\\((([^ \\)]+)( |)){1}([^\\)]+)*\\)"
# When match, than it is not a procedure header.
# ^\(define[ ]+[^\(]
# shellcheck disable=SC2034
tokenNotAProcedureHeader="^\\(define[ ]+[^\\(]"

# Marks the beginning of an parameter description block.
# \s*Parameters:\s*
# shellcheck disable=SC2034
tokenParamBlockStart="^[[:space:]]*Parameters:[[:space:]]*"
# Marks the end of an parameter description block.
# ^\s*$
# shellcheck disable=SC2034
tokenParamBlockEnd="^[[:space:]]*$"
# Marks the beginning of a parameter description.
# ^\s*([^ ]+)[ ]+-[ ]+(.*)$
# shellcheck disable=SC2034
tokenParameterBegin="^[[:space:]]*([^ ]+)[ ]+-[ ]+(.*)$"
# Checks the procedure header if the procedure has optional parameters.
# ^.*\.\s+args\s*$
# shellcheck disable=SC2034
tokenParamHasOptional="^.*\\.[[:space:]]+args[[:space:]]*$"

# Marks the parameters attributes string.
# ^\[([^]]+)\]\s*(.*)
# shellcheck disable=SC2034
tokenParamAttributes="^\\[([^]]+)\\][[:space:]]*(.*)"
# Marks the parameter attribute "optional".
# \s*optional\s*
# shellcheck disable=SC2034
tokenParamAttrIsOptional="^[[:space:]]*optional[[:space:]]*"
# Marks the parameter attribute "default".
# ^\s*default:\s*(.*)\s*$
# shellcheck disable=SC2034
tokenParamAttrDefault="^[[:space:]]*default:[[:space:]]*(.*)[[:space:]]*$"

# Marks the beginning of a "Returns" block.
# ^\s*Returns:\s*(.*)
# shellcheck disable=SC2034
tokenReturnsBlockStart="^[[:space:]]*Returns:[[:space:]]*(.*)"

# Marks the start of an export block.
# ^\(export([ ]+\(list|[ ]*\n)
# shellcheck disable=SC2034
tokenExportBlockStart="^\\(export([ ]+\\(list|[ ]*\\n)"
# Marks the end of an export block.
# (^\)\)|\)\)[^\)]|\)\)$)
# shellcheck disable=SC2034
tokenExportBlockEnd="(^\\)\\)|\\)\\)[^\\)]|\\)\\)$)"
# Marks an export block (one line).
# ^\s*\(export[ ]+\(list[ ]+(\(list[ ]+'[^ ]+[ ]+[^\)]+\)([ ]+|))*\)\)\s*$
# shellcheck disable=SC2034
tokenExportBlock="^[[:space:]]*\\(export[ ]+\\(list[ ]+(\\(list[ ]+'[^ ]+[ ]+[^\\)]+\\)([ ]+|))*\\)\\)[[:space:]]*$"
# Marks the export definitions in the export string.
# ^\s*\(export\s*\(list\s*(.*)\s*\)\)\s*$
# shellcheck disable=SC2034
tokenExportDefinitions="^[[:space:]]*\\(export[[:space:]]*\\(list[[:space:]]*(.*)[[:space:]]*\\)\\)[[:space:]]*$"
# Marks the export name and procedure name in an export definition.
# ^\s*list\s*('([^ ]+)|"([^ ]+)")[ ]+([^ )]+)\s*\)\s*$
# shellcheck disable=SC2034
tokenExportNameProcName="^[[:space:]]*list[[:space:]]*('([^ ]+)|\"([^ ]+)\")[ ]+([^ )]+)[[:space:]]*\\)[[:space:]]*$"




#

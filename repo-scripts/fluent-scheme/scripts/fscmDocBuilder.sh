#!/bin/bash

#*******************************************************************************
# global variables
#*******************************************************************************

#*******************************************************************************
# load dependencies
#*******************************************************************************

#*******************************************************************************
# Generate the overview documentation file, which is the entry point for the
# generated documentation.
#
# parameters:
#   $1 - [required] The path to the source code directory (where the
#        customization files are located).
#   $2 - [required] The path to the documentation directory, which is where the
#        API documentation will be generated.
#   $3 - [required] The path to the directory where the intermediate format is
#        located.
#   $4 - [required] The name of the file in the intermediate format where the
#        attributes for a source file are saved, normally "description".
#   $5 - [required] The path to the repository directory.
#   $6 - [required] The maximum length of the short description. A description
#        string that is longer will be truncated. Used in the tables to show an
#        overview and give a short description.
#
# returns:
#   0 on success, not 0 otherwise.
#*******************************************************************************
function generateOverviewDoc {
  local srcDir=${1:?}
  local docDir=${2:?}
  local tmpDir=${3:?}
  local descriptionFileName=${4:?}
  local repoDir=${5:?}
  local shortDescrLength=${6:?}
  local overviewFile="$docDir/README.md"
  local docLinkPath=${docDir##"$repoDir"}
  local moduleName=""
  local moduleDescrFile="$srcDir/module-description.md"
  local srcFilesListDescrFile="$srcDir/source-files-list-description.md"
  local expProcsListDescrFile="$srcDir/exported-procs-list-description.md"
  local privateProcsListDescrFile="$srcDir/private-procs-list-description.md"
  local exportedProcsCount=0
  local privateProcsCount=0

  # Get module name.
  moduleName=$(basename $repoDir) || return $?
  
  echo "  processing overview file"
  # ----------------------------------------------------------------------------
  # Heading
  echo "# Module: $moduleName" >> $overviewFile
  
  if [[ -f $moduleDescrFile ]]; then
    cat $moduleDescrFile >> $overviewFile
  else
    local text=""
    text+="This is the overview of the module documentation. In this file you " \
    text+="can find a list with all source files of the module, a list of all " \
    text+="exported procedures of the module and a list of all module private " \
    text+="procedures. The name of each list entry links to the specific " \
    text+="documentation of the source file, exported procedure or private " \
    text+="procedure.  "
    echo "$text" >> $overviewFile
  fi
  echo "" >> $overviewFile

  # ----------------------------------------------------------------------------
  # Source files table.
  echo "## Source Files" >> $overviewFile
  
  if [[ -f $srcFilesListDescrFile ]]; then
    cat $srcFilesListDescrFile >> $overviewFile
  else
    local text=""
    text+="A list of all source files of this module.  "
    echo "$text" >> $overviewFile
  fi
  echo "" >> $overviewFile
  
  echo "| Name | Description |" >> $overviewFile
  echo "|:---- |:----------- |" >> $overviewFile
  
  for srcFileDbDir in "$tmpDir"/*; do
    local srcFileName=""
    local descrFilePath="$srcFileDbDir/$descriptionFileName"
    local descrFileContents=""

    # Read source file description parameters to associative array.
    descrFileContents="$(cat $descrFilePath)" || return $?
    declare -A srcFileParams="($descrFileContents)"

    # Get source file name.
    srcFileName=$(basename $srcFileDbDir) || return $?

    # Truncate file description if too long.
    local fileDescr=${srcFileParams[description]}
    fileDescr=${fileDescr//\\\\n/ }
    fileDescr=${fileDescr//\\\*/*}
    if [[ ${#fileDescr} -gt $shortDescrLength ]]; then
      fileDescr="${fileDescr:0:$shortDescrLength}..."
    fi

    # Write table entry.
    local line=""
    line+="| [:page_facing_up: $srcFileName]($docLinkPath/${srcFileName}.md) "
    line+="| $fileDescr |"
    echo $line >> $overviewFile
  done
  echo "" >> $overviewFile

  # ----------------------------------------------------------------------------
  # Exported procedures table.
  echo "## Exported Procedures" >> $overviewFile
  
  if [[ -f $expProcsListDescrFile ]]; then
    cat $expProcsListDescrFile >> $overviewFile
  else
    local text=""
    text+="A list of all exported procedures of this module. The procedure " \
    text+="names are the internal procedure names. Normally this is the same " \
    text+="as the exported name, prepended with the import name, but thy can " \
    text+="differ. Look into the detailed procedure documentation to get the " \
    text+="export name of the procedure. Exported procedures are imported " \
    text+="into a source file with the \`import\` procedure (see the " \
    text+="fscm-module-manager module for further details), which takes the " \
    text+="import name as the first parameter. Therefore every exported " \
    text+="procedure of a module will be imported according to the following " \
    text+="syntax:  "
    echo "$text" >> $overviewFile
    echo "\`<import name>/<export name of the procedure>\`.  " >> $overviewFile
  fi
  echo "" >> $overviewFile
  
  for srcFileDbDir in "$tmpDir"/*; do
    local srcFileName=""
    
    # Recreate associative arrays.
    unset procNameHeaderIdMap
    unset procHeaderIdsCount
    declare -A procNameHeaderIdMap
    declare -A procHeaderIdsCount

    # Get source file name.
    srcFileName=$(basename $srcFileDbDir) || return $?

    if [[ -d "$srcFileDbDir/procs" ]] &&
       [[ -f "$srcFileDbDir/procs"/0.proc ]]; then
      for procFilePath in "$srcFileDbDir/procs"/*.proc; do
        local procFileContents=""
        local id=""
        
        # Read properties of procedure into associative array.
        procFileContents="$(cat $procFilePath)" || return $?
        declare -A procFileProps="($procFileContents)"
        
        # Generate procedure heading ids.
        id=$(getHeaderId "${procFileProps[name]}") || return $?
        
        # Append id count if id already exists.
        if ! [[ -z ${procHeaderIdsCount[$id]} ]]; then
          _$((procHeaderIdsCount[$id]++))
          id="$id-${procHeaderIdsCount[$id]}"
        else
          procHeaderIdsCount[$id]=0
        fi
        procNameHeaderIdMap[${procFileProps[name]}]=$id

        # Only add entry to table if procedure is exported.
        if [[ ${procFileProps[isExported]} ]] &&
           [[ ${procFileProps[isExported]} = "yes" ]]; then
          _=$((exportedProcsCount++))
          
          # Print table header when first procedure.
          if [[ $exportedProcsCount -eq 1 ]]; then 
            echo "| Name | Description |" >> $overviewFile
            echo "|:---- |:----------- |" >> $overviewFile
          fi
          
          # Truncate procedure description if too long.
          local procDescr=${procFileProps[description]}
          procDescr=${procDescr//\\n/}
          procDescr=${procDescr//\\\*/*}
          if [[ ${#procDescr} -gt $shortDescrLength ]]; then
            procDescr="${procDescr:0:$shortDescrLength}..."
          fi

          # Write table entry.
          local line=""
          line+="| [:page_facing_up: ${procFileProps[name]}]"
          line+="($docLinkPath/${srcFileName}.md#"
          line+="${procNameHeaderIdMap[${procFileProps[name]}]}) | $procDescr |"
          echo $line >> $overviewFile
        fi
      done
    fi
  done
  
  # Check if there were any exported procedures.
  if [[ $exportedProcsCount -le 0 ]]; then
    echo "  " >> $overviewFile
    echo "The module does not export any procedure.  " >> $overviewFile
    echo "  " >> $overviewFile
  fi
  
  echo "" >> $overviewFile

  # ----------------------------------------------------------------------------
  # Module private procedures table.
  echo "## Private Procedures" >> $overviewFile
  
  if [[ -f $privateProcsListDescrFile ]]; then
    cat $privateProcsListDescrFile >> $overviewFile
  else
    local text=""
    text+="A list of all private procedures of this module. This procedures " \
    text+="are not accessible from outside the module.  "
    echo "$text" >> $overviewFile
  fi
  echo "" >> $overviewFile
  
  for srcFileDbDir in "$tmpDir"/*; do
    local srcFileName=""
    
    # Recreate associative arrays.
    unset procNameHeaderIdMap
    unset procHeaderIdsCount
    declare -A procNameHeaderIdMap
    declare -A procHeaderIdsCount
    
    # Get source file name.
    srcFileName=$(basename $srcFileDbDir) || return $?

    if [[ -d "$srcFileDbDir/procs" ]] &&
       [[ -f "$srcFileDbDir/procs"/0.proc ]]; then
      for procFilePath in "$srcFileDbDir/procs"/*.proc; do
        local procFileContents=""
        local id=""
        
        # Read properties of procedure into associative array.
        procFileContents="$(cat $procFilePath)" || return $?
        declare -A procFileProps="($procFileContents)"
        
        # Generate procedure heading ids.
        id=$(getHeaderId "${procFileProps[name]}") || return $?
        
        # Append id count if id already exists.
        if ! [[ -z ${procHeaderIdsCount[$id]} ]]; then
          _$((procHeaderIdsCount[$id]++))
          id="$id-${procHeaderIdsCount[$id]}"
        else
          procHeaderIdsCount[$id]=0
        fi
        procNameHeaderIdMap[${procFileProps[name]}]=$id

        # Only add entry to table if procedure is not exported.
        if ! [[ ${procFileProps[isExported]} ]] ||
           [[ ${procFileProps[isExported]} = "no" ]]; then
          _=$((privateProcsCount++))

          # Print table header when first procedure.
          if [[ $privateProcsCount -eq 1 ]]; then 
            echo "| Name | Description |" >> $overviewFile
            echo "|:---- |:----------- |" >> $overviewFile
          fi

          # Truncate procedure description if too long.
          local procDescr=${procFileProps[description]}
          procDescr=${procDescr//\\n/}
          procDescr=${procDescr//\\\*/*}
          if [[ ${#procDescr} -gt $shortDescrLength ]]; then
            procDescr="${procDescr:0:$shortDescrLength}..."
          fi

          # Write table entry.
          local line=""
          line+="| [:page_facing_up: ${procFileProps[name]}]"
          line+="($docLinkPath/${srcFileName}.md#"
          line+="${procNameHeaderIdMap[${procFileProps[name]}]}) | $procDescr |"
          echo $line >> $overviewFile
        fi
      done
    fi
  done
  
  # Check if there were any private procedures.
  if [[ $privateProcsCount -le 0 ]]; then
    echo "  " >> $overviewFile
    echo "The module does not have any private procedures.  " >> $overviewFile
    echo "  " >> $overviewFile
  fi
  
  echo "" >> $overviewFile
}

#*******************************************************************************
# Generate documentation files for each source file.
#
# parameters:
#   $1 - [required] The path to the documentation directory, which is where the
#        API documentation will be generated.
#   $2 - [required] The path to the directory where the intermediate format is
#        located.
#   $3 - [required] The name of the file in the intermediate format where the
#        attributes for a source file are saved, normally "description".
#   $4 - [required] The path to the repository directory.
#   $5 - [required] The maximum length of the short description. A description
#        string that is longer will be truncated. Used in the tables to show an
#        overview and give a short description.
#   $6 - [optional, default: "no"] Only the values "yes" and "no" are accepted
#        by this parameter. When "yes" is passed than the module for which the
#        documentation is generated exports it's public procedures global.
#        Therefore the exported procedures have no import name. So the import
#        name mark will not be generated by the function when passed "yes".
#
# returns:
#   0 on success, not 0 otherwise.
#*******************************************************************************
function generateFileDoc {
  local docDir=${1:?}
  local tmpDir=${2:?}
  local descriptionFileName=${3:?}
  local repoDir=${4:?}
  local shortDescrLength=${5:?}
  local procsGloballyExported=${6-"no"}
  local overviewFile="$docDir/README.md"
  local docLinkPath=${docDir##"$repoDir"}

  for srcFileDbDir in "$tmpDir"/*; do
    local srcFileName=""
    local docFile=""
    local descrFilePath="$srcFileDbDir/$descriptionFileName"
    local descrFileContents=""
    local fileDescr=""
    local exportsCount=0
    local procsCount
    local procIdsGenerated="no"
    
    # Recreate associative arrays.
    unset procNameHeaderIdMap
    unset procHeaderIdsCount
    declare -A procNameHeaderIdMap
    declare -A procHeaderIdsCount

    # Read source file description parameters to associative array.
    descrFileContents="$(cat $descrFilePath)" || return $?
    declare -A srcFileParams="($descrFileContents)"

    # Get source file name.
    srcFileName=$(basename $srcFileDbDir) || return $?
    docFile="$docDir/${srcFileName}.md"
    echo "  processing ${srcFileName}"
    
    # Count number of procedures.
    shopt -s nullglob
    local procFiles=("$srcFileDbDir/procs"/*.proc)
    shopt -u nullglob
    procsCount=${#procFiles[@]}
    
    # --------------------------------------------------------------------------
    # Heading
    echo ":arrow_left: [Go back to the overview.]($docLinkPath/README.md)" >> $docFile
    echo "  " >> $docFile
    echo "  " >> $docFile
    echo "  " >> $docFile
    echo "  " >> $docFile
    echo "# File: ${srcFileParams[name]}" >> $docFile
    fileDescr=$(stringDesanitize "${srcFileParams[description]}") || return $?
    fileDescr=${fileDescr//\\\*/*}
    echo "$fileDescr" >> $docFile
    echo "" >> $docFile
    echo ":arrow_right: [Go to source code of this file.](${srcFileParams[linkPath]})"\
      >> $docFile
    echo "" >> $docFile

    # --------------------------------------------------------------------------
    # Exported procedures table.
    if [[ ${srcFileParams[hasExports]} = "yes" ]]; then
      echo "## Exported Procedures" >> $docFile
      echo "| Name | Description |" >> $docFile
      echo "|:---- |:----------- |" >> $docFile
      
      if [[ -d "$srcFileDbDir/procs" ]] &&
         [[ -f "$srcFileDbDir/procs"/0.proc ]]; then
        for procFilePath in "$srcFileDbDir/procs"/*.proc; do
          local procFileContents=""
          local procName=""
          
          # Read properties of procedure into associative array.
          procFileContents="$(cat $procFilePath)" || return $?
          declare -A procFileProps="($procFileContents)"
          procName="${procFileProps[name]}"
          
          # Generate procedure heading ids.
          if [[ $procIdsGenerated = "no" ]]; then
            local id=""
            id=$(getHeaderId "$procName") || return $?
            
            # Append id count if id already exists.
            if ! [[ -z ${procHeaderIdsCount[$id]} ]]; then
              _$((procHeaderIdsCount[$id]++))
              id="$id-${procHeaderIdsCount[$id]}"
            else
              procHeaderIdsCount[$id]=0
            fi
            procNameHeaderIdMap[$procName]=$id
          fi
          
          # Only add entry to table if procedure is exported.
          if [[ ${procFileProps[isExported]} ]] &&
             [[ ${procFileProps[isExported]} = "yes" ]]; then
            # Truncate procedure description if too long.
            local procDescr=${procFileProps[description]}
            procDescr=${procDescr//\\n/}
            procDescr=${procDescr//\\\*/*}
            if [[ ${#procDescr} -gt $shortDescrLength ]]; then
              procDescr="${procDescr:0:$shortDescrLength}..."
            fi

            # Write table entry.
            local line=""
            line+="| [:page_facing_up: ${procFileProps[name]}]"
            line+="(#${procNameHeaderIdMap[$procName]}) | $procDescr |"
            echo $line >> $docFile
            
            _=$((exportsCount++))
          fi
        done
        procIdsGenerated="yes"
      fi
      echo "" >> $docFile
    fi
    
    # --------------------------------------------------------------------------
    # Module private procedures table.
    if [[ $exportsCount -lt $procsCount ]]; then
      echo "## Private Procedures" >> $docFile
      echo "| Name | Description |" >> $docFile
      echo "|:---- |:----------- |" >> $docFile

      if [[ -d "$srcFileDbDir/procs" ]] &&
         [[ -f "$srcFileDbDir/procs"/0.proc ]]; then
        for procFilePath in "$srcFileDbDir/procs"/*.proc; do
          local procFileContents=""
          local procName=""

          # Read properties of procedure into associative array.
          procFileContents="$(cat $procFilePath)" || return $?
          declare -A procFileProps="($procFileContents)"
          procName="${procFileProps[name]}"
          
          # Generate procedure heading ids.
          if [[ $procIdsGenerated = "no" ]]; then
            local id=""
            id=$(getHeaderId "$procName") || return $?
            
            # Append id count if id already exists.
            if ! [[ -z ${procHeaderIdsCount[$id]} ]]; then
              _$((procHeaderIdsCount[$id]++))
              id="$id-${procHeaderIdsCount[$id]}"
            else
              procHeaderIdsCount[$id]=0
            fi
            procNameHeaderIdMap[$procName]=$id
          fi

          # Only add entry to table if procedure is not exported.
          if ! [[ ${procFileProps[isExported]} ]] ||
             [[ ${procFileProps[isExported]} = "no" ]]; then
            # Truncate procedure description if too long.
            local procDescr=${procFileProps[description]}
            procDescr=${procDescr//\\n/}
            procDescr=${procDescr//\\\*/*}
            if [[ ${#procDescr} -gt $shortDescrLength ]]; then
              procDescr="${procDescr:0:$shortDescrLength}..."
            fi

            # Write table entry.
            local line=""
            line+="| [:page_facing_up: ${procFileProps[name]}]"
            line+="(#${procNameHeaderIdMap[$procName]}) | $procDescr |"
            echo $line >> $docFile
          fi
        done
        procIdsGenerated="yes"
      fi
      echo "" >> $docFile
    fi

    # --------------------------------------------------------------------------
    # Procedures details.
    if [[ $procsCount -gt 0 ]]; then
      local isFirst="yes"
      echo "## Procedure Documentation" >> $docFile

      if [[ -d "$srcFileDbDir/procs" ]] &&
         [[ -f "$srcFileDbDir/procs"/0.proc ]]; then
        for procFilePath in "$srcFileDbDir/procs"/*.proc; do
          local procFileContents=""
          local procSyntaxString=""
          local paramsDbDir="$srcFileDbDir/params"

          # Read properties of procedure into associative array.
          procFileContents="$(cat $procFilePath)" || return $?
          declare -A procFileProps="($procFileContents)"
          
          # Generate procedure heading ids.
          if [[ $procIdsGenerated = "no" ]]; then
            local id=""
            id=$(getHeaderId "$procName") || return $?
            
            # Append id count if id already exists.
            if ! [[ -z ${procHeaderIdsCount[$id]} ]]; then
              _$((procHeaderIdsCount[$id]++))
              id="$id-${procHeaderIdsCount[$id]}"
            else
              procHeaderIdsCount[$id]=0
            fi
            procNameHeaderIdMap[$procName]=$id
          fi
          
          echo "" >> $docFile
          if [[ $isFirst = "no" ]]; then
            echo "-------------------------------------------------" >> $docFile
          else
            isFirst="no"
          fi
          echo "### ${procFileProps[name]}" >> $docFile
          echo "" >> $docFile
          
          echo "#### Syntax" >> $docFile
          echo "\`\`\`scheme" >> $docFile
          procSyntaxString=$(genProcSyntaxString "$paramsDbDir" \
            "${procFileProps[name]}" "${procFileProps[parameterIds]}") \
            || return $?
          echo "$procSyntaxString" >> $docFile
          echo "\`\`\`" >> $docFile
          echo "" >> $docFile
          local linkLine=""
          linkLine+=":arrow_right: [Go to source code of this procedure.]"
          linkLine+="(${srcFileParams[linkPath]}#L${procFileProps[lineNumber]})"
          echo "$linkLine" >> $docFile
          echo "" >> $docFile
          
          if [[ ${procFileProps[isExported]} == "yes" ]]; then
            echo "#### Export Name" >> $docFile
            if [[ $procsGloballyExported = "yes" ]]; then
              echo "\`${procFileProps[exportName]}\`" >> $docFile
            else
              echo "\`<import-name>/${procFileProps[exportName]}\`" >> $docFile
            fi
            echo "" >> $docFile
          fi
          
          echo "#### Description" >> $docFile
          local procDescr=""
          procDescr=$(stringDesanitize "${procFileProps[description]}") \
            || return $?
          procDescr=${procDescr//\\\*/*}
          echo "$procDescr" >> $docFile
          echo "" >> $docFile
          
          if [[ -n ${procFileProps[parameterIds]} ]]; then
            echo "#### Parameters" >> $docFile
            unset IFS
            for paramId in ${procFileProps[parameterIds]}; do
              paramFileContents=$(cat "$paramsDbDir/$paramId.param") \
                || return $?
              declare -A paramProps="($paramFileContents)"
              
              # Parameter name.
              echo "##### \`${paramProps[name]}\`  " >> $docFile
              
              # Parameter attributes.
              if [[ ${paramProps[isOptional]} = "yes" ]] || 
                 [[ ${paramProps[hasDefault]} = "yes" ]]; then
                local attrsString=""
                attrsString="_Attributes:"
                
                if [[ ${paramProps[isOptional]} = "yes" ]]; then
                  attrsString+=" optional"
                fi
                
                if [[ ${paramProps[hasDefault]} = "yes" ]]; then
                  local defVal=""
                  defVal=$(stringDesanitize "${paramProps[defaultValue]}" "no") \
                    || return $?
                  attrsString+=", default: \`$defVal\`"
                fi
                
                attrsString+="_  "
                echo "$attrsString" >> $docFile
              fi
              
              # Parameter description.
              local paramDescr=""
              paramDescr=$(stringDesanitize "${paramProps[description]}") \
                 || return $?
              paramDescr=${paramDescr//\\\*/*}
              echo "$paramDescr" >> $docFile
              echo "" >> $docFile
            done
            echo "" >> $docFile
          fi
          
          if [[ -n ${procFileProps[returns]} ]]; then
            echo "#### Returns" >> $docFile
            
            local returnsDescr=""
            returnsDescr=$(stringDesanitize "${procFileProps[returns]}") \
               || return $?
            returnsDescr=${returnsDescr//\\\*/*}
            echo "$returnsDescr" >> $docFile
          fi
        done
        procIdsGenerated="yes"
      fi
      echo "" >> $docFile
    fi
  done
}

#*******************************************************************************
# Generates the procedure syntax string, e.g.: 
# "(procedure-name param1 param2 [optional-param])"
#
# parameters:
#   $1 - [required] The path to the parameters table from the intermediate
#        format of the current source file.
#   $1 - [required] The name of the procedure.
#   $1 - [optional, default: ""] The parameter id's of the parameter table
#        entries (the parameters of the procedure).
#
# returns:
#   The procedure syntax string.
#*******************************************************************************
function genProcSyntaxString {
  local paramsDbDir=${1:?}
  local procName=${2:?}
  local procParamIds=${3-""}
  local syntax="($procName"
  local paramFileContents=""
  
  unset IFS
  for paramId in $procParamIds; do
    paramFileContents=$(cat "$paramsDbDir/$paramId.param") || return $?
    declare -A paramProps="($paramFileContents)"
    
    if [[ ${paramProps[isOptional]} = "yes" ]]; then
      syntax+=" [${paramProps[name]}]"
    else
      syntax+=" ${paramProps[name]}"
    fi
  done
  syntax+=")"
  
  # Return the generated procedure syntax string.
  echo $syntax
}

#*******************************************************************************
# Generates the markdown header link id from the header string.
#
# parameters:
#   $1 - [required] The header string from which the link id should be created.
#
# returns:
#   The generated header link id.
#*******************************************************************************
function getHeaderId {
  local headerString=${1:?}
  local id=""
  local secondMatchNotEmpty="true"
  local nonWordRegex="([a-zA-Z0-9-]*)[^a-zA-Z0-9-]+([a-zA-Z0-9-]*)"
  
  id=${headerString,,} # Convert to lowercase.
  id=${id// /-} # Convert spaces to hyphens.
  id=${id//--/-} # Convert multiple hyphens into one hyphen.
  
  # Remove any non word characters.
  while [[ $secondMatchNotEmpty = "true" ]]; do
    if [[ $id =~ $nonWordRegex ]]; then
      id="${BASH_REMATCH[1]}${BASH_REMATCH[2]}"
      if [[ -z ${BASH_REMATCH[2]} ]]; then
        secondMatchNotEmpty="false"
      fi
    else
      secondMatchNotEmpty="false"
    fi
  done
  
  # Return the generated id.
  echo "$id"
}

#*******************************************************************************
# Desanitizes the passed string (removing escape characters).
#
# parameters:
#   $1 - [optional, default: ""] The string to desanitize.
#   $2 - [optional, default: "no"] Only the values "yes" and "no" are accepted
#        by this parameter. When "yes" is passed, then newline characters will
#        be removed from the string.
#
# retuns:
#   The desanitized string.
#*******************************************************************************
function stringDesanitize() {
  local string=${1-""}
  local noNewLine=${2-"no"}
  
  if [[ $noNewLine = "yes" ]]; then
    string=${string//\\n/}
  else
    string=${string//\\n/$'\n'}
  fi
  
  string=${string//\\\\/\\}
  string=${string//\\\$/\$}
  string=${string//\\\`/\`}
  string=${string//\\\'/\'}
  string=${string//\\\"/\"}
  
  # Return the desanitized string.
  echo "$string"
}

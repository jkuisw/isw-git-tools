#!/bin/bash

#*******************************************************************************
# global variables
#*******************************************************************************
scriptDir=$(cd "$(dirname $0)" && pwd)
scriptDir=$(readlink -f "$scriptDir")
repoDir=$(readlink -f "$scriptDir/..")
moduleName=""
oldModuleVersion=""
newModuleVersion=""
productionBranch="master"
skipCi="no"

#*******************************************************************************
# load dependencies
#*******************************************************************************
# shellcheck source=../../../scripts/semver/semver.sh
source "$scriptDir/../../../scripts/semver/semver.sh"
# shellcheck source=../../../scripts/git/gitIsCurrentBranch.sh
source "$scriptDir/../../../scripts/git/gitIsCurrentBranch.sh"
# shellcheck source=../../../scripts/git/gitCheckForChanges.sh
source "$scriptDir/../../../scripts/git/gitCheckForChanges.sh"
# shellcheck source=../../../scripts/git/gitNewVersionTag.sh
source "$scriptDir/../../../scripts/git/gitNewVersionTag.sh"

#*******************************************************************************
# main function
#*******************************************************************************
function main {
  local compResult
  
  # Parse command line parameters.
  parseParameters "$@"

  echo "#######################################################################"
  echo "Create new fluent scheme module version."
  echo "#######################################################################"
  echo "Settings"
  echo "  -> Module Version: $newModuleVersion"
  echo "  -> Repository Directory: $repoDir"
  echo "  -> Production Branch: $productionBranch"
  echo "  -> Skip CI: $skipCi"
  
  # Parse the module name from the repository directory.
  moduleName=$(basename $repoDir) || exit 1
  isValidModuleName $moduleName
  if [ $? -eq 1 ]; then
    echo "The module name \"$moduleName\" is invalid! Maybe passed the wrong" \
      "repository directory?"
    echo "repository directory: \"$repoDir\""
    printHelp
    exit 1
  fi

  # Check if there are changes in the git repository.
  gitCheckForChanges $repoDir
  if [ $? -eq 1 ]; then
    echo "There are changes in the repository. Please commit all changes" \
      "before creating a new version."
    printHelp
    exit 1
  fi

  # Check if on production branch.
  gitIsCurrentBranch $productionBranch $repoDir
  if [ $? -eq 1 ]; then
    echo "The current branch is not \"$productionBranch\". Please checkout" \
      "the \"$productionBranch\" branch before creating a new version."
    printHelp
    exit 1
  fi

  echo "Creating new version \"$newModuleVersion\" for the fluent scheme" \
    "module \"$moduleName\""
  echo "in the directory \"$repoDir\"."

  # Get old package version, if there is one.
  oldModuleVersion=$(findOldModuleVersion $repoDir $moduleName) || exit 1

  # Check if the new version is newer than the old (current) module version.
  if [ ! -z $oldModuleVersion ]; then
    echo "Found existing fluent scheme module version \"$oldModuleVersion\"."

    # Old version exists, therefore check if old version is really older than
    # the new version.
    compResult=$(semverCompare $oldModuleVersion $newModuleVersion)

    if [[ $compResult -eq 1 ]]; then
      # New version is not newer.
      echo "The requested new module version is not newer than the current" \
        "module version."
      echo "  -> requested module version = $newModuleVersion"
      echo "  -> current module version = $oldModuleVersion"
      printHelp
      exit 1
    fi
  fi

  # Create or update module version file "module-version.scm".
  echo "Updating version information for the fluent scheme module."
  createUpdateVersionFile $repoDir $moduleName $newModuleVersion || exit 1

  # Create the new git tag.
  echo "Creating the git tag for the new version and pushing changes to the" \
    "remote repository."
  gitNewVersionTag $newModuleVersion "$repoDir" $skipCi || exit 1

  # Finished sucessfully.
  echo "Finished sucessfully."
}

#*******************************************************************************
# parse parameters
#*******************************************************************************
function parseParameters {
  #-----------------------------------------------------------------------------
  # Parse option arguments.
  while getopts ":hsr:b:" opt; do
    case $opt in
      h)
        printHelp
        exit 0
        ;;
      s)
        skipCi="yes"
        ;;
      r)
        # Check if repository directory exists.
        if [[ ! -d $OPTARG ]]; then
          echo "The repository directory \"$OPTARG\" does not exist."
          printUsage
          exit 1
        fi
        repoDir=$(readlink -f "$OPTARG")
        ;;
      b)
        productionBranch=$OPTARG
        ;;
      \?)
        echo "Invalid option: -$OPTARG"
        printUsage
        exit 1
        ;;
      :)
        echo "Option -$OPTARG requires an argument."
        printUsage
        exit 1
        ;;
    esac
  done

  #-----------------------------------------------------------------------------
  # Parse non-option arguments.
  shift $((OPTIND - 1))

  #.............................................................................
  # Parse the new module version (required parameter).
  # Check if module version was given.
  if [ -z "$1" ]; then
    echo "Need a new module version."
    printUsage
    exit 1
  fi

  # Check if new module version is valid according to the semantic versioning
  # specification: http://semver.org/
  semverIsValid $1
  if [[ $? -eq 1 ]]; then
    echo "The version string \"$1\" is invalid! "
    echo "Please follow the semantic versioning specification:" \
      "http://semver.org/"
    printUsage
    exit 1
  fi

  newModuleVersion="$1"
}

#*******************************************************************************
# Check if valid module name.
#*******************************************************************************
function isValidModuleName {
  local moduleNamePattern="^[^\\/\\.]+$"

  if [[ $1 =~ $moduleNamePattern ]]; then
    # Valid module name.
    return 0
  fi

  # Invalid module name.
  return 1
}

#*******************************************************************************
# Search for old package version in the given directory.
#*******************************************************************************
function findOldModuleVersion {
  local repoDirectory=$1
  local reqModuleName=$2
  local versionFileName="module-version.scm"
  local regexPattern="\\(define\\s+module\\.([^\\.]+)\\.version\\s+\\\"([^\"]+)\\\"\\)"
  local tempModuleName
  local tempOldModuleVersion

  if [[ -e $repoDirectory/$versionFileName ]]; then
    while read line; do
      if [[ $line =~ $regexPattern ]]; then
        tempModuleName=${BASH_REMATCH[1]}
        tempOldModuleVersion=${BASH_REMATCH[2]}

        # Check if the module name is the requested one.
        if [ "$tempModuleName" == "$reqModuleName" ]; then
          # Found module, check if valid version.
          semverIsValid $tempOldModuleVersion
          if [[ $? -eq 0 ]]; then
            # Found valid old package version.
            echo $tempOldModuleVersion
            return 0
          fi
        fi
      fi
    done < $repoDirectory/$versionFileName
  fi

  # No old package version found.
  echo ""
  return 1
}

#*******************************************************************************
# Create or update the "module-version.scm" file with the new version.
#*******************************************************************************
function createUpdateVersionFile {
  local dir=$1
  local name=$2
  local newVersion=$3

  # Delete version file if it exists and create it.
  rm -f "$dir/module-version.scm" || return 1
  touch "$dir/module-version.scm" || return 1

  # Write data to the version file.
  echo "(define module.$name.version \"$newVersion\")" >> "$dir/module-version.scm"
}

#*******************************************************************************
# TODO
#*******************************************************************************


#*******************************************************************************
# Prints the usage to sdtout.
#*******************************************************************************
function printUsage {
  echo "USAGE"
  echo "    createVersion.sh [-h] [-s] [-r <path>] [-b <branch>] <module-version>"
  echo ""
}

#*******************************************************************************
# Prints the help to stdout.
#*******************************************************************************
function printHelp {
  echo ""

  echo "NAME"
  echo "    createVersion.sh - Create a new fluent scheme repository version."
  echo ""

  printUsage

  echo "DESCRIPTION"
  echo "    This script creates a new fluent scheme module version. This is done"
  echo "    by updateing or creating the module-version.scm file containing the"
  echo "    new version of the module. Than commiting the changes to the git"
  echo "    repository, creating a new git version tag and finally pushing the"
  echo "    changes to the remote repository. The module name is the name of the"
  echo "    repository root directory, which must be a valid scheme variable "
  echo "    name without the point character (\".\"). Because the module name is"
  echo "    also a directory name, only valid characters for directories without"
  echo "    the point character can be used for the module name. The required"
  echo "    parameters are:"
  echo "      <module-version> - The new version of the fluent scheme module."
  echo ""

  echo "OPTIONS"
  echo "    -h"
  echo "      Display this help and exit."
  echo ""
  echo "    -s"
  echo "      To every git commit message the string \"[skip ci]\" will be"
  echo "      prepended if the flag is passed. This is needed when the skript"
  echo "      runs in an gitlab ci job to avoid looping ci jobs."
  echo ""
  echo "    -r <path>"
  echo "      The path to the directory of the module repository. Should be an"
  echo "      absolute path or realtive from the location of the current working"
  echo "      directory."
  echo ""
  echo "    -b <branch>"
  echo "      The git branch which is used for productive code (not for "
  echo "      development) normally this is the \"master\" branch."
  echo ""
}

#*******************************************************************************
# Call main function and pass parameters.
#*******************************************************************************
main "$@"

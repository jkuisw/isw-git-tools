Language Support for Fluent Scheme
==================================
This directory provides some scripts and templates for a fluent scheme
repository (the main language of the repository). These are scripts for
versioning (`createVersion.sh`), API documentation generation (
`fscmDocGenerator.sh`) and packaging of a fluent scheme module (
`packageFscmModule.sh`). Each of the three bash scripts bring their own
documentation with them. Therefore if you call them with the `-h` flag you get a
description of what the script does and the possible parameters for
configuration (e.g.: `./createVersion.sh -h`). The templates are a configuration
file and wrappers for the scripts, that provide meaningful default configuration
and inject the settings from the configuration file.

Add Language Support for Fluent Scheme
--------------------------------------
To add language support for fluent scheme to a repository you have to create a
directory where you store the templates (preferably `scripts` at repository
root) and then copy them into this folder:

```shell
cd "<repository directory>"
mkdir "scripts"
cp "submodules/isw-git-tools/repo-scripts/fluent-scheme/templates"/*.sh "scripts/"
```

> **Note:** In the code snippet above `<repository directory>` must be replaced
> with the path to the repository where you want to add the language support.
> Furthermore it is assumed that you have added the `isw-git-tools` repository
> (which is this repository) as a submodule to the repository where you want to
> add the language support.

Now you have to configure the language support. Therefore open the just before
copied `config.sh` file in the `scripts` directory and edit the settings in the
configuration section. The configuration options are explained in the comments
of the file.  

The next step is to add a `.gitignore` file to the root directory of your
repository with the following content:

```
*.ss~
*.ss#*
.#*.ss

*.scm~
*.scm#*
.#*.scm

cleanup-fluent*.sh
```

Every file that matches a pattern defined above will be ignored by git. This
avoids adding temporary files to the version control.

Finally commit your changes to the repository by the following commands:

```shell
git add --all
git commit -m "add and configure language support for fluent scheme"
```

Usage of the Script Wrappers
----------------------------
### `exCreateVersion.sh` - Script Wrapper
Creates a new version of the module by passing the new version string to it. The
version string has to be valid according to the
[semantic versioning specification](http://semver.org/). Example (assuming that
you are in the repository root directory):  

```shell
./scripts/exCreateVersion.sh "0.0.1"
```

### `generateDoc.sh` - Script Wrapper
Creates the API documentation for the module by parsing the source code comments
(more in the section [How to Document the Code](#how-to-document-the-code) for
automatic documentation generation). Example (assuming that you are in the
repository root directory):  

```shell
./scripts/generateDoc.sh
```

### `packageModule.sh` - Script Wrapper
Generates the API documentation and a new version of the module. It accepts two
parameters and operates in two different modes. If the second parameter is
omitted or an empty string, than the script operates in mode 1, otherwise in
mode 2. 

**Parameters:**
* skipCi - [optional, default: "no"] When passed "yes" to this parameter, then
  to every commit message produced by the script the text `"[skip ci]"` is
  prepended, which avoids circular execution of CI jobs. This is crucial when
  called in CI jobs, but can be omitted when called local.
* tag - [optional, default: ""] When this parameter is passed, than the script
  operates in mode 2. The value must be the name of an existing git tag and the
  tag name should be a version string following the
  [semantic versioning specification](http://semver.org/). When the tag name is
  not a valid version, then the script will do nothing.

#### Mode 1 - Create Package from last Commit Message
In this mode the last commit message must include a special command, otherwise
the script will do nothing. The syntax for the special command is as follows
(including the square brackets):

```shell
[[create version <version string>]]
```

Where `<version string>` is the new version for the module and must follow the
[semantic versioning specification](http://semver.org/). For the sake of
completeness, below is the regular expression that is used to check the last
commit message for the command and retrieving the version string:

```shell
.*\[\[create\s+version\s+(\"|)([^] \"]+)(\s*|\"\s*)\]\].*
```

This command is meant to be used in CI jobs to automate packaging of the 
modules, see the section 
[How to Setup the GitLab CI for Module Packaging](#how-to-setup-the-gitlab-ci-for-module-packaging).
Anyway, here is an example of how to call the wrapper (assuming that you are
in the repository root directory):  

```shell
./scripts/packageModule.sh "no"
```

#### Mode 2 - Create Package from Git Tag
In this mode the version of the module is defined by the git tag passed. The
script will delete that tag, generate the API documentation, commit the changes
and then recreate the tag. This command is meant to be used in CI jobs to
automate packaging of the modules, see the section 
[How to Setup the GitLab CI for Module Packaging](#how-to-setup-the-gitlab-ci-for-module-packaging).
Anyway, here is an example of how to call the wrapper (assuming that you are
in the repository root directory):  

```shell
./scripts/packageModule.sh "no" "<tag name>"
```

How to Document the Code
------------------------
The API documentation is generated by parsing so called
**_documentation blocks_** from the source files. A **_documentation block_**
is a series of scheme comment lines (a scheme comment starts with the `";"`
character and ends at the end of the line) with the first comment line starting,
after the `";"` character, with at least four stars. Furthermore no characters
before the semicolon (`";"`) are allowed (except for spaces). The
**_documentation block_** ends with the first non comment block line, which is a
line that does not follow the rules defined above. Example 
**_documentation block_**:

```scheme
;*******************************************************************************
; A documentation block.
```

The first line of a **_documentation block_** will always be ignored by the API
documentation, it is only for identifying the beginning of a
**_documentation block_**. There are three different types of documentation
blocks supported, which are:
* File description block
* Procedure documentation block
* Export block

### The File Description Block
This **_documentation block_** contains the file description. It must be the
first **_documentation block_** of the file and ends with an empty line after
the last comment line.

### The Procedure Documentation Block
The next non empty line after the last comment line of the procedure
**_documentation block_** must be a procedure header or at least the beginning
of a procedure header, than the **_documentation block_** is a procedure
**_documentation block_**. A procedure header is the definition of a scheme
procedure with it's parameters, for example:

```scheme
; Procedure header with two parameters.
(define (procedure-name parameter-1 parameter-2)

; Procedure header with two parameters and optional parameters.
(define (procedure-name parameter-1 parameter-2 . args)
```

The procedure header can be split into multiple lines. The next code snippet
shows an example for a procedure documentation block:

```scheme
;*******************************************************************************
; Executes the given system command and returns the output of this system
; command as an object list. System commands are commands that will be executed
; on the current shell where fluent was started, see the fluent documentation
; of the scheme command `system`.
;
; Parameters:
;   system-command - A string with the system command, that will be executed on
;     the current shell.
;   execdir - [optional, default: #f] A path to the directory where the command
;     should be executed. If omitted, than the current directory will be used.
;
; Returns: The output of the system command as an object list or false in case
;   of an error.
(define (sys-cmd-output-to-object-list system-command . args)
```

As you can see, a procedure documentation is divided into three sub-blocks:
1. The procedure description sub-block starts at the beginning of the 
   documentation block (the second comment line, because as mentioned above the
   first comment line is ignored) and ends before the next sub-block (parameter
   sub-block or returns sub-block) or, if there is no other sub-block, at the
   last comment line of the documentation block. This sub-block is
   **mandatory**.
2. The parameters description sub-block starts with a comment line which text
   is only `"Parameters:"` (leading and trailing whitespace is allowed but
   ignored) and it ends before the returns sub-block or, if there is no returns
   sub-block, at the last comment line of the documentation block. It describes
   the parameters of the procedure. This sub-block is **mandatory if the
   procedure has parameters** and **MUST NOT be used when the procedure has no
   parameters**.
3. The returns sub-block starts with a comment line which contains the text
   `"Returns:"` at the beginning of that line (leading and trailing whitespace is
   allowed but ignored) and ends at the last comment line of the documentation
   block. It describes the return value of the procedure and is **optional**.

#### The Parameters Description Sub-Block
The first line of a this Sub-Block will always be ignored by the API
documentation, it is only for identifying it. The remaining lines of the block
should contain a list of parameter descriptions, following the syntax:

```
<parameter name> - <parameter attributes> <parameter description>
```

Explanation of the tokens used in the syntax above:
##### `<parameter name>` - Token
Is the name of the parameter and must correspond to the name in the procedure
headers parameter list. When the parameter name is optional it is good practice
to use the variable name, which you use in the procedure for the optional
parameter, as parameter name. Though this will not be checked by the
documentation parser. This token is **mandatory**.

##### `<parameter attributes>` - Token
Is a list of parameter attributes that must comply with the syntax (the order of
the attributes is NOT mandatory):

```
[<non value attribute>, <value attribute>: <attribute value>]
```

As you can see, an attributes list starts with the `"["` (opening square bracket)
character and ends with the `"]"` (closing square bracket) and attributes are
separated by the string `", "` (a comma and at least one space). Furthermore
there are two types of attributes: 
* *non value attribute* - This is just a flag, if present, than it's set, 
  otherwise not.
* *value attribute* - Is an attribute with an value and the attribute tag is
  separated from the value with the string `": "` (a colon and at least one 
  space). Everything after the separator string (`": "`) until the next
  attributes separator string (`", "`) or the end of the attributes list (`"]"`)
  will be the attributes value.  

> **Note:** The attributes separator string `", "` **MUST NOT** be a part of the
> attributes value. 

Currently the following attributes are supported:

| Attribute Tag |    Attribute Type     | Description                          |
| :----------:  | :-------------------: | :----------------------------------- |
| `optional`    | *non value attribute* | When present, than the parameter is optional. Mandatory for optional parameters and if the `default` attribute is present. Must not be present when the parameter is not optional. |
| `default`     | *value attribute*     | The default value of an optional Parameter. Mandatory when the `optional` attribute is present and if the parameter is optional. Must not be present when the parameter is not optional |

##### `parameter description` - Token
The parameter description, can be multiple lines and ends before the next 
parameters or the end of the parameters description sub-block. This token is
**mandatory**.

### The Export Block
The next lines after the last comment line of the export
**_documentation block_** must be a valid export definition (no empty line!),
than the **_documentation block_** is an export **_documentation block_**. The
comment lines of an export block are ignored by the API documentation. The
export block just marks an export definition to be parsed. This is needed, so
that the documentation generator knows which procedures are exported with which
name.

### General Documentation Notes
In general, the GitLab Flavored Markdown syntax can be used in the
**_documentation blocks_** to format the output. For a description of the
GitLab Flavored Markdown syntax
[see the GitLab Flavored Markdown documentation](https://docs.gitlab.com/ee/user/markdown.html).

> :warning: In the Markdown syntax two or more spaces in a row at the end of a
> line results in a line break. But some editors delete trailing whitespaces by
> default (whitespace = spaces `" "`, new line `"\n"`, carriage return `"\r"`
> and tabulator `"\t"`) and this could destroy the format of the API
> documentation. Therefore check if your editor deletes trailing whitespaces and
> deactivate it for scheme scripts.

### Example Source File Documentation
```scheme
;*******************************************************************************
; Example file description.

;*******************************************************************************
; This procedure does some stuff and has two mandatory parameter.
;
; Parameters:
;   param1 - Description for the first mandatory parameter.
;   param2 - Description for the second mandatory parameter.
;
; Returns: This procedure returns the string `"a value string"`.
(define (do-some-stuff param1 param2) (let (
    (a-local-variable "a value string")
  )
  ; Do some stuff.
  
  ; Return the value of the local variable.
  a-local-variable
))

;*******************************************************************************
; This procedure does some other stuff. It has two mandatory and one optional
; parameter.
;
; Parameters:
;   param1 - Description for the first mandatory parameter.
;   param2 - Description for the second mandatory parameter.
;   optional-parameter - [optional, default: "default value"] Description for
;     the optional parameter.
;
; Returns: This procedure returns the string `"another string"`.
(define (do-some-other-stuff param1 param2 . args) (let (
    (a-local-variable "another string")
    (optional-parameter "default value")
  )
  ; Parse optional parameter.
  (if (> (length args) 0) (begin
    ; The optional parameter was passed, therefore get it's value.
    (set! optional-parameter (list-ref args 0))
  ))
  
  ; Do some other stuff.
  
  ; Return the value of the local variable.
  a-local-variable
))

;*******************************************************************************
; Export public procedures.
(export (list
  (list 'do-some-stuff do-some-stuff)
  (list 'do-some-other-stuff do-some-other-stuff)
))
```

### Customization of the Documentation Output
The documentation generator creates an overview file which has in general the
following structure:
* [**customizable**] General module description text.
* [**customizable**] Source files list description text.
* A list of all source files of the module.
* [**customizable**] Exported procedures list description text.
* A list of all exported procedures of the module.
* [**customizable**] Private procedures list description text.
* A list of all private procedures of the module.

Each of the sections above that are marked with [**customizable**] can be
configured. To do this you have to create a file with a special name and the
customized text within it and save it into the source folder (the folder which
is passed as `-s` argument to the `generateDoc.sh` command). The documentation
generator will look into the source folder for that files. If it can find a file
it will use the contents of the file as the text, otherwise a default text will
be used. The following sections document the different possible customizations.

#### General Module Description Text
+ *File name:* `module-description.md`  
+ *Default text:*  
  This is the overview of the module documentation. In this file you can find a
  list with all source files of the module, a list of all exported procedures of
  the module and a list of all module private procedures. The name of each list
  entry links to the specific documentation of the source file, exported
  procedure or private procedure.

#### Source Files List Description Text
+ *File name:* `source-files-list-description.md`  
+ *Default text:*  
  A list of all source files of this module.

#### Exported Procedures List Description Text
+ *File name:* `exported-procs-list-description.md`  
+ *Default text:*  
  A list of all exported procedures of this module. The procedure names are the
  internal procedure names. Normally this is the same as the exported name,
  prepended with the import name, but thy can differ. Look into the detailed
  procedure documentation to get the export name of the procedure. Exported
  procedures are imported into a source file with the `import` procedure (see 
  the `fscm-module-manager` module for further details), which takes the import 
  name as the first parameter. Therefore every exported procedure of a module
  will be imported according to the following syntax:  
  `<import name>/<export name of the procedure>`

#### Private Procedures List Description Text
+ *File name:* `private-procs-list-description.md`  
+ *Default text:*  
  A list of all private procedures of this module. This procedures are not
  accessible from outside the module.

How to Setup the GitLab CI for Module Packaging
-----------------------------------------------
In this section the configuration of the GitLab continuous integration (CI) for
a repository to execute the module packaging script will be explained. As
described [above](#packagemodulesh-script-wrapper), the `packageModule.sh`
script generates the API documentation of a fluent scheme module and furthermore
creates a new version of the module. So first an environment (operating system)
is needed which can execute the script. For this a linux distribution running in
a [docker](https://www.docker.com/) container is used. 

> **Note:** If you don't know what docker is, please read "Part 1: Orientation"
> and "Part 2: Containers" of the
> [docker get started guide](https://docs.docker.com/get-started/).

In your fluent scheme repository create the following docker file and save it
under a meaningful directory (e.g.: `docker/fedora26-git`): 

```Docker
FROM fedora:26
RUN dnf -y update && dnf -y install git bash which openssh
CMD ["/bin/bash"]
```

With this docker file you create an image based on the official fedora 26 docker
image and with the following additional software installed:
* git
* bash
* which
* openssh

The first two are needed by the packaging script, the last two are needed to
push the results back to the repository (details below). Therefore create a
docker image from the docker file as described in the
[docker get started guide](https://docs.docker.com/get-started/) and register
it to the GitLab repository (see the docker registry section of the repository
on the GitLab homepage).  
Now there is an environment to execute the script, so the next step is to
configure the CI to do so. 

> **Note:** If you don't know what the Gitlab CI is and what it does, please
> read at least the following 
> [Getting started with GitLab CI](https://docs.gitlab.com/ee/ci/quick_start/README.html)
> guide. Additional information can be found on the general
> [GitLab Continuous Integration (GitLab CI)](https://docs.gitlab.com/ee/ci/README.html)
> site.

Because the packaging script changes files in the repository, it needs to push
them back to the remote repository. But for security reasons, the docker image
which will be executed by the CI can only pull from the remote repository, not
push. For this to work an SSH connection to GitLab must be set up in the docker
image. Therefore create a new SSH key pair. The public key must be added as
"Deploy Key" to the project and the private key must be saved as secret variable
in the project.  
For the "Deploy Key" go the the settings page of the repository
and than to the sub menu "Repository". There should be a section named
"Deploy Keys", where the public key can be added (name it for example: "public
key for CI jobs", 
[see this page](https://docs.gitlab.com/ce/ssh/README.html#deploy-keys) for
further information on how to add deploy keys). Don't forget to make the deploy
key writable, because you want to push changes to the repository with this key.  
Now for the private key as well go to the settings page and then to the sub
menu "CI / CD". There should be a section named "Secret Variables". Choose for
the name `SSH_PRIVATE_KEY` and paste the private key into the value field (
[see this page](https://docs.gitlab.com/ce/ci/variables/README.html#secret-variables)
for further information on secret variables).  
Now you have everything configured to setup the SSH connection in the docker
image. The next step is to actually configure the CI, which is done by the
following `yaml` script (which will also setup the SSH connection in the docker
image).

> **Note:** For further information on how to configure CI jobs with the
> `.gitlab-ci.yml` file see the
> [Configuration of your jobs with .gitlab-ci.yml](https://docs.gitlab.com/ee/ci/yaml/)
> guide.

```yaml
variables:
  GIT_SUBMODULE_STRATEGY: recursive
  PACKAGE_MODULE_COMMIT_MESSAGES_SKIP_CI: "yes"

stages:
  - build
  - test
  - package
  - deploy

before_script:
  # Install ssh-agent if not already installed, it is required by Docker.
  # (change apt-get to yum if you use a CentOS-based image)
  - 'which ssh-agent || ( dnf update -y && dnf install openssh -y )'
  
  # Run ssh-agent (inside the build environment)
  - eval $(ssh-agent -s)
  
  # Add the SSH key stored in SSH_PRIVATE_KEY variable to the agent store
  - ssh-add <(echo "$SSH_PRIVATE_KEY")
  
  # For Docker builds disable host key checking. Be aware that by adding that
  # you are suspectible to man-in-the-middle attacks.
  # WARNING: Use this only with the Docker executor, if you use it with shell
  # you will overwrite your user's SSH config.
  - mkdir -p ~/.ssh
  - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config'
  
  # In order to properly check the server's host key, assuming you created the
  # SSH_SERVER_HOSTKEYS variable previously, uncomment the following two lines
  # instead.
  #- mkdir -p ~/.ssh
  #- '[[ -f /.dockerenv ]] && echo "$SSH_SERVER_HOSTKEYS" > ~/.ssh/known_hosts'

package_module_triggered_by_commit:
  stage: package
  image: <path to docker image>
  tags:
    - docker
  only:
    - master
  script:
    - git checkout -B "$CI_COMMIT_REF_NAME"
    - git remote set-url origin "git@gitlab.com:${CI_PROJECT_PATH}.git"
    - git config --global user.email "$GITLAB_USER_EMAIL"
    - git config --global user.name "$GITLAB_USER_NAME"
    - ./scripts/packageModule.sh "$PACKAGE_MODULE_COMMIT_MESSAGES_SKIP_CI"
    
package_module_triggered_by_tag:
  stage: package
  image: <path to docker image>
  tags:
    - docker
  only:
    - tags
  script:
    - git checkout -B "master"
    - git remote set-url origin "git@gitlab.com:${CI_PROJECT_PATH}.git"
    - git config --global user.email "$GITLAB_USER_EMAIL"
    - git config --global user.name "$GITLAB_USER_NAME"
    - ./scripts/packageModule.sh "$PACKAGE_MODULE_COMMIT_MESSAGES_SKIP_CI" "$CI_COMMIT_TAG"
```

### Script Configuration
The only thing in this script that must be adopted is the path to the docker
image which should be used for the execution of the CI job. The two points where
the path should be inserted is marked by `<path to docker image>` in the
sections `package_module_triggered_by_commit` and
`package_module_triggered_by_tag`. Use the path to the docker image that you
have previously registered to the repository (though any valid docker path could
be used, e.g. a docker image from the official docker hub).

> **Note:** If the package module script wrapper wasn't copied to the `scripts`
> directory, than the script call has to be adjusted too (last line of script).
> Furthermore the configuration assumes that your production branch is `master`
> and that your are following the workflow described in the
> [Development Workflow](/guides/dev-workflow.md) guide.

### What is going on in the script?
The `before_script` section configures the SSH connection of the docker image
to GitLab by using the previously defined secret variable `SSH_PRIVATE_KEY`,
which is passed by the GitLab CI to the docker image. The `before_script`
commands will be executed before every job. Furthermore two jobs are configured:
* `package_module_triggered_by_commit`
* `package_module_triggered_by_tag`

#### The `package_module_triggered_by_commit` Job
This job will be triggered when a commit is pushed to the master branch. The
commands in the `script` section will then checkout the master branch (by
default the repository is pulled into the docker image in a detached head
state) and add GitLab as a remote repository. Furthermore telling git the user
which has triggered the CI job. After that the package script will finally be
executed, which will be called with only one parameter (skipCi = "yes").
Therefore the module will only be packaged when the last commit message includes
the according command to do so (see [above](#packagemodulesh-script-wrapper)).

#### The `package_module_triggered_by_tag` Job
The job will be triggered when a git tag is created. The commands in the
`script` section will then checkout the master branch (by default the repository
is pulled into the docker image in a detached head state) and add GitLab as a
remote repository. Furthermore telling git the user which has triggered the CI
job. After that the package script will finally be executed, which will be
called with two parameters. The first one to avoid circular CI jobs (skipCi = 
"yes") and the second is the name of the git tag which triggered this job. If
the tag name is a valid version according to the
[semantic versioning specification](http://semver.org/), then the module will be
packaed (see [above](#packagemodulesh-script-wrapper)).

### Final Steps
Save all changes, add and commit them to the repository and push the changes to
GitLab.

```shell
git add --all
git commit -m "configure repository CI to package module"
git push
```





















<!-- -->

#!/bin/bash

scriptDir=$(cd "$(dirname $0)" && pwd)
scriptDir=$(readlink -f "$scriptDir")

# shellcheck source=./config.sh
source "$scriptDir/config.sh"

# For a description of the parameters for the fscmDocGenerator.sh command, see
# the help by calling "fscmDocGenerator.sh -h".
"${scriptPath:?}"/fscmDocGenerator.sh \
  -r "${repositoryDirectory:?}" \
  -d "${documentationDirectory:?}" \
  -s "${sourceDirectory:?}" 

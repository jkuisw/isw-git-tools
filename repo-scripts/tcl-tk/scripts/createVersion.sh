#!/bin/bash

#*******************************************************************************
# global variables
#*******************************************************************************
scriptDir=$(cd "$(dirname $0)" && pwd)
scriptDir=$(readlink -f "$scriptDir")
pkgDir=$(readlink -f "$scriptDir/..")
pkgName=""
oldPkgVersion=""
newPkgVersion=""
productionBranch="master"

#*******************************************************************************
# load dependencies
#*******************************************************************************
# shellcheck source=../../../scripts/semver/semver.sh
source "$scriptDir/../../../scripts/semver/semver.sh"
# shellcheck source=../../../scripts/git/gitIsCurrentBranch.sh
source "$scriptDir/../../../scripts/git/gitIsCurrentBranch.sh"
# shellcheck source=../../../scripts/git/gitCheckForChanges.sh
source "$scriptDir/../../../scripts/git/gitCheckForChanges.sh"
# shellcheck source=../../../scripts/git/gitNewVersionTag.sh
source "$scriptDir/../../../scripts/git/gitNewVersionTag.sh"

#*******************************************************************************
# main function
#*******************************************************************************
function main {
  local compResult
  local versionRegex
  local tclVersionString

  # Parse command line parameters.
  parseParameters "$@"

  # Check if there are changes in the git repository.
  gitCheckForChanges $pkgDir
  if [ $? -eq 1 ]; then
    echo "There are changes in the repository. Please commit all changes" \
      "before creating a new version."
    printHelp
    exit 1
  fi

  # Check if on production branch.
  gitIsCurrentBranch $productionBranch $pkgDir
  if [ $? -eq 1 ]; then
    echo "The current branch is not \"$productionBranch\". Please checkout" \
      "the \"$productionBranch\" branch before creating a new version."
    printHelp
    exit 1
  fi

  echo "Creating new version \"$newPkgVersion\" for the Tcl/Tk package" \
    "\"$pkgName\" in the directory \"$pkgDir\"."

  # Get old package version, if there is one.
  oldPkgVersion=$(findOldPackageVersion $pkgDir $pkgName)
  echo "Found existing Tcl/Tk package version \"$oldPkgVersion\"."

  # In Tcl/Tk only major, minor and patch parts of the version can be used.
  # Therefore extract it.
  versionRegex="^(0|[1-9][0-9]*)\.(0|[1-9][0-9]*)\.(0|[1-9][0-9]*)"
  if [[ $newPkgVersion =~ $versionRegex ]]; then
    tclVersionString=${BASH_REMATCH[0]}
  else
    echo "Cannot extract major, minor and patch version from $newPkgVersion"
    printHelp
    exit 1
  fi

  # Create or update Tcl/Tk package directory and version file.
  if [ -z $oldPkgVersion ]; then
    echo "Creating new Tcl/Tk package directory."
    mkdir $pkgDir/${pkgName}${tclVersionString}
  else
    # Old version exists, therefore check if old version is really older than
    # the new version.
    compResult=$(semverCompare $oldPkgVersion $newPkgVersion)

    if [[ $compResult -eq -1 ]]; then
      # New version is newer, therefor proceed with renaming the package
      # directory.
      echo "Updating Tck/Tck package directory."
      mv $pkgDir/${pkgName}${oldPkgVersion} \
        $pkgDir/${pkgName}${tclVersionString}
    else
      # New version is not newer.
      echo "The requested new package version is not newer than the current" \
        "package version."
      echo "  -> requested package version = $newPkgVersion"
      echo "  -> current package version = $oldPkgVersion"
      printHelp
      exit 1
    fi
  fi

  # Create or update Tcl/Tk "packageVersion.tcl" file.
  echo "Updating version information for the Tcl/Tk package."
  createUpdateVersionFile \
    $pkgDir/${pkgName}${tclVersionString} \
    $pkgName \
    $tclVersionString

  # Make Tcl/Tk package index.
  echo "Building the Tcl/Tk package index."
  tclsh "$scriptDir/mkPackageIndex.tcl" \
    "$pkgDir" \
    "$pkgName" \
    "$tclVersionString"

  # Create the new git tag.
  echo "Creating the git tag for the new version and pushing changes to the" \
    "remote repository."
  gitNewVersionTag $newPkgVersion $pkgDir

  # Finished sucessfully.
  echo "Finished sucessfully."
}

#*******************************************************************************
# parse parameters
#*******************************************************************************
function parseParameters {
  #-----------------------------------------------------------------------------
  # Parse option arguments.
  while getopts ":hf:b:" opt; do
    case $opt in
      h)
        printHelp
        exit 0
        ;;
      f)
        # Check if package directory exists.
        if [[ ! -d $OPTARG ]]; then
          echo "The package directory \"$OPTARG\" does not exist."
          printUsage
          exit 1
        fi
        pkgDir=$(readlink -f "$OPTARG")
        ;;
      b)
        productionBranch=$OPTARG
        ;;
      \?)
        echo "Invalid option: -$OPTARG"
        printUsage
        exit 1
        ;;
      :)
        echo "Option -$OPTARG requires an argument."
        printUsage
        exit 1
        ;;
    esac
  done

  #-----------------------------------------------------------------------------
  # Parse non-option arguments.
  shift $((OPTIND - 1))

  #.............................................................................
  # Parse the package name (required parameter).
  # Check if package name was given.
  if [ -z "$1" ]; then
    echo "Need a package name."
    printUsage
    exit 1
  fi

  # Check if package name is valid.
  isValidPackageName $1
  if [[ $? -eq 1 ]]; then
    echo "pkgName = $1"
    echo "Not a valid package name. Valid characters for a package name" \
      "are: A-Z, a-z, -, _"
    printUsage
    exit 1
  fi

  pkgName="$1"

  #.............................................................................
  # Parse the new package version (required parameter).
  shift

  # Check if package version was given.
  if [ -z "$1" ]; then
    echo "Need a new package version."
    printUsage
    exit 1
  fi

  # Check if new package version is valid according to the semantic versioning
  # specification: http://semver.org/
  semverIsValid $1
  if [[ $? -eq 1 ]]; then
    echo "The version string \"$1\" is invalid! "
    echo "Please follow the semantic versioning specification:" \
      "http://semver.org/"
    printUsage
    exit 1
  fi

  newPkgVersion="$1"
}

#*******************************************************************************
# Check if valid package name.
#*******************************************************************************
function isValidPackageName {
  local pkgNamePattern="^[A-Za-z_-]*$"

  if [[ $1 =~ $pkgNamePattern ]]; then
    # Valid package name.
    return 0
  fi

  # Invalid package name.
  return 1
}

#*******************************************************************************
# Search for old package version in the given directory.
#*******************************************************************************
function findOldPackageVersion {
  local pkgDirectory=$1
  local reqPkgName=$2
  local pkgNameVerPattern="([A-Za-z_-]*)(.*)"
  local dirName
  local tempPkgName
  local tempOldPkgVersion

  for pd in $pkgDirectory/*; do
    dirName=$(basename $pd)
    if [[ $dirName =~ $pkgNameVerPattern ]]; then
      tempPkgName=${BASH_REMATCH[1]}
      tempOldPkgVersion=${BASH_REMATCH[2]}

      # Check if package name is the requested one.
      if [ "$tempPkgName" == "$reqPkgName" ]; then
        # Found package, check if valid version.
        semverIsValid $tempOldPkgVersion
        if [[ $? -eq 0 ]]; then
          # Found valid old package version.
          echo $tempOldPkgVersion
          return 0
        fi
      fi
    fi
  done

  # No old package version found.
  echo ""
  return 1
}

#*******************************************************************************
# Create or update the "packageVersion.tcl" file with the new version.
#*******************************************************************************
function createUpdateVersionFile {
  local dir=$1
  local name=$2
  local newVersion=$3

  # Delete version file if it exists and create it.
  rm -f "$dir/packageVersion.tcl"
  touch "$dir/packageVersion.tcl"

  # Write data to the version file.
  echo "set pkgVersion $newVersion" >> "$dir/packageVersion.tcl"
  echo "set pkgName $name" >> "$dir/packageVersion.tcl"
}

#*******************************************************************************
# TODO
#*******************************************************************************


#*******************************************************************************
# Prints the usage to sdtout.
#*******************************************************************************
function printUsage {
  echo "USAGE"
  echo "    createVersion.sh [-h] [-f <path>] [-b <branch>] <pkg-name>" \
    "<pkg-version>"
  echo ""
}
#:hf:b
#*******************************************************************************
# Prints the help to stdout.
#*******************************************************************************
function printHelp {
  echo ""

  echo "NAME"
  echo "    createVersion.sh - Create a new Tcl/Tk repository version."
  echo ""

  printUsage

  echo "DESCRIPTION"
  echo "    This script creates a new Tcl/Tk package version. This is done by"
  echo "    updateing or creating the package directory and the packageVersion.tcl"
  echo "    file. Than building a new package index, commiting the changes to"
  echo "    the git repository, creating a new git version tag and finally"
  echo "    pushing the changes to the remote repository. The required parameters"
  echo "    are:"
  echo "      <pkg-name> - The name of the Tcl/Tk package."
  echo "      <pkg-version> - The new version of the Tcl/Tk package."
  echo ""

  echo "OPTIONS"
  echo "    -h"
  echo "      Display this help and exit."
  echo ""
  echo "    -f <PATH>"
  echo "      The path to the directory which contains the Tcl/Tk package "
  echo "      directory (e.g.: \"icemops0.0.1\"), should be an absolute path or"
  echo "      realtive from the location of the current working directory."
  echo ""
  echo "    -b <branch>"
  echo "      The git branch which is used for productive code (not for "
  echo "      development) normally this is the \"master\" branch."
  echo ""
}

#*******************************************************************************
# Call main function and pass parameters.
#*******************************************************************************
main "$@"

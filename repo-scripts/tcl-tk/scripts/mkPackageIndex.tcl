# This Tcl/Tk script generates a package inedx of the package in the given
# directory.
#
# Parameters:
#   pkgDir - The directory which contains the package directory.
#   pkgName - The package name.
#   pkgVersion - The package version.

package require Tcl 8.4

set pkgDir [lindex $argv 0]
set pkgName [lindex $argv 1]
set pkgVersion [lindex $argv 2]

#set versionRegex {^(0|[1-9][0-9]*)\.(0|[1-9][0-9]*)\.(0|[1-9][0-9]*)}
#regexp $versionRegex $pkgVersion versionStr
set pkgPath [file normalize "$pkgDir/$pkgName$pkgVersion"]
pkg_mkIndex -verbose $pkgPath

#!/bin/bash

newVersion=$1
scriptDir=$(cd "$(dirname $0)" && pwd)
scriptDir=$(readlink -f "$scriptDir")

# shellcheck source=./config.sh
source "$scriptDir/config.sh"

# For a description of the parameters for the createVersion.sh command, see the
# help by calling "createVersion.sh -h".
${scriptPath:?}/createVersion.sh \
  -f ${packageDir:?} \
  -b ${productionBranch:?} \
  ${packageName:?} \
  $newVersion

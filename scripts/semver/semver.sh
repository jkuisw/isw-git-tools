#!/bin/bash

SEMVER_REGEX="^(0|[1-9][0-9]*)\.(0|[1-9][0-9]*)\.(0|[1-9][0-9]*)(\-[0-9A-Za-z-]+(\.[0-9A-Za-z-]+)*)?(\+[0-9A-Za-z-]+(\.[0-9A-Za-z-]+)*)?$"

#*******************************************************************************
# Check if the given version string is a valid according to the semantic
# versioning specification 2.0.0 (see http://semver.org/).
#
# parameters:
#   $1 - [required] The version string to check.
#   $2 - [optional] The name of a variable where to store the parsed version.
#
# returns:
#   0 when valid, 1 otherwise.
#*******************************************************************************
function semverIsValid {
  local version=$1
  if [[ "$version" =~ $SEMVER_REGEX ]]; then
    # if a second argument is passed, store the result in var named by $2
    if [ "$#" -eq "2" ]; then
      local major=${BASH_REMATCH[1]}
      local minor=${BASH_REMATCH[2]}
      local patch=${BASH_REMATCH[3]}
      local prere=${BASH_REMATCH[4]}
      local build=${BASH_REMATCH[5]}
      eval "$2=(\"$major\" \"$minor\" \"$patch\" \"$prere\" \"$build\")"
    fi
    return 0
  else
    return 1
  fi
}

#*******************************************************************************
# Compares two semantic versioning version strings. Prints to stdout 0 if both
# are equal, -1 if second version (second parameter) is newer and 1 otherwise.
#
# parameters:
#   $1 - [required] First version string.
#   $2 - [required] Second version string.
#
# returns:
#   0 on success, not 0 otherwise.
#*******************************************************************************
function semverCompare {
  semverIsValid "$1" V
  semverIsValid "$2" V_

  # MAJOR, MINOR and PATCH should compare numericaly
  for i in 0 1 2; do
    local diff=$((${V[$i]} - ${V_[$i]}))
    if [[ $diff -lt 0 ]]; then
      echo -1; return 0
    elif [[ $diff -gt 0 ]]; then
      echo 1; return 0
    fi
  done

  # PREREL should compare with the ASCII order.
  if [[ -z "${V[3]}" ]] && [[ -n "${V_[3]}" ]]; then
    echo 1; return 0;
  elif [[ -n "${V[3]}" ]] && [[ -z "${V_[3]}" ]]; then
    echo -1; return 0;
  elif [[ -n "${V[3]}" ]] && [[ -n "${V_[3]}" ]]; then
    if [[ "${V[3]}" > "${V_[3]}" ]]; then
      echo 1; return 0;
    elif [[ "${V[3]}" < "${V_[3]}" ]]; then
      echo -1; return 0;
    fi
  fi

  echo 0
}

#!/bin/bash

#*******************************************************************************
# Checks if the given branch is the current one. Must be executed in a git
# repository or the directory passed as the second parameter must be in a git
# repository, otherwise it will fail.
#
# parameters:
#   $1 - [required] The requested branch.
#   $2 - [optional, default: current directory] Any directory which belongs to
#        the git repository.
#
# returns:
#   0 if the current branch is equal to the requested, 1 otherwise.
#*******************************************************************************
function gitIsCurrentBranch {
  local reqBranch=$1
  local repoDir=$2
  local tmpDir
  local currBranch

  # Initialize directory variables.
  if [ -z "$repoDir" ]; then 
    repoDir=$(readlink -f ".") || exit 1 
  fi
  tmpDir=$(pwd)

  # Cd into repository and get current branch.
  cd $repoDir || exit 1
  currBranch=$(git rev-parse --abbrev-ref HEAD) || exit 1
  if [ "$reqBranch" == "$currBranch" ]; then
    # Current branch is the requested one.
    cd $tmpDir || exit 1
    return 0
  fi

  # Current branch is not the requested one.
  cd $tmpDir || exit 1
  return 1
}

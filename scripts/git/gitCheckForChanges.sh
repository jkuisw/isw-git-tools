#!/bin/bash

#*******************************************************************************
# Checks if there are changes in the current repository. When no parameters are
# passed, than the current directory must be in a git repository (which is the 
# one that will be checked). If a directory is passed to the first parameter, 
# than this directory must be in a git repository (which is the on that will be 
# checked). 
# If non of the above conditions are met, than it will fail. 
# No changes mean: The working tree is clean (git status).
#
# parameters:
#   $1 - [optional, default: current directory] Any directory which belongs to
#        the git repository.
#
# returns:
#   0 if there are no changes, 1 otherwise.
#*******************************************************************************
function gitCheckForChanges {
  local repoDir=$1
  local tmpDir

  # Initialize directory variables.
  if [ -z "$repoDir" ]; then 
    repoDir=$(readlink -f ".") || exit 1 
  fi
  tmpDir=$(pwd)

  # Cd into repository and check for changes.
  cd $repoDir || exit 1
  if [[ $(git status --porcelain) ]]; then
    # There are changes in the repository.
    cd $tmpDir || exit 1
    return 1
  fi

  # No changes in the repository.
  cd $tmpDir || exit 1
  return 0
}

#!/bin/bash

#*******************************************************************************
# Creates a new git version tag. Steps:
#   1) Add all changes to the staging area.
#   2) Commit all changes.
#   3) Create the new tag.
#   4) Push all changes to the remote repository.
# Must be executed in a git repository or the directory passed as the second
# parameter must be in a git repository, otherwise it will fail.
#
# parameters:
#   $1 - [required] The version string for the tag.
#   $2 - [optional, default: current directory] Any directory which belongs to
#        the git repository.
#   $3 - [optional, default: "no"] When "yes" than the string "[skip ci]" will
#        be prepended to every commit message.
#
# returns:
#   0 on success, 1 otherwise.
#*******************************************************************************
function gitNewVersionTag {
  local version=${1:?}
  local repoDir=$2
  local skipCi=${3-"no"}
  local skipCiString=""
  local commitMessage=""
  local tmpDir

  # Initialize directory variables.
  if [ -z "$repoDir" ]; then 
    repoDir=$(readlink -f ".") || return 1
  fi
  tmpDir=$(pwd)
  
  # Check if skip ci string should be prepended to commit messages. 
  if [[ $skipCi = "yes" ]]; then
    skipCiString="[skip ci] "
  fi
  
  # Cd into repository.
  cd $repoDir || return 1

  # Add all changes to the staging area.
  git add --all || return 1

  # Commit all changes.
  commitMessage="${skipCiString}Changes for the new version \"${version}\"."
  # Commit should be successful even if there were no changes.
  git commit -m "$commitMessage" || true

  # Create the new tag.
  commitMessage="${skipCiString}Created new version \"${version}\"."
  # Commit should be successful even if there were no changes.
  git tag "$version" -m "$commitMessage" || true

  # Push all changes to the remote repository.
  git push --all || return 1
  git push --tags || return 1

  # Cd back to caller directory.
  cd $tmpDir || return 1
  return 0
}

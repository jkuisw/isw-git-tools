Setup GitLab Access
===================

Prerequisites
-------------

In this guide it is assumed, that you fulfill the following prerequisites:
* The git version control system is installed (can be checked with the command 
  `git --version`), if not [see the git homepage][git-download]. On Linux, git
  is normally installed out of the box. On Windows you have to install it.
* Basic knowledge of the git version control system, especially the git 
  commands: `git clone`, `git status`, `git pull`, `git push`, `git add` and 
  `git commit` `git checkout` `git branch`. If not, than you can read the really
  quick guide [git - the simple guide][git-simple-guide] or download the 
  [git cheatsheet][git-cheatsheet] or somewhat more comprehensively the official
  git documentation [Getting Started - Git Basics][git-basics] or just ask
  google. 
* The following git global settings are properly configured (needs to be done 
  once for any operating system user account where you want to use git and 
  should be your user account): 
  + `git config --global user.name "<your name>"`
  + `git config --global user.email "<your e-mail>"`
* You have an GitLab account, if not [create one][gitlab-register].
* You are a member of the [GitLab group jkuisw][jkuisw-group]. If not, contact
  the group owner (click on the link and go to the group members section to find
  the group owner).

SSH Gitlab Access
-----------------
When this is the first time you configure the GitLab SSH access, than you have
probably already noticed the orange bar at the top when you open a repository
on the GitLab homepage. This means that you don't have any SSH key pairs added
yet to your profile. In order to use GitLab you have to add SSH key pairs to
**your** GitLab profile in the settings section. Furthermore for every computer
(specifically for every operating systems user account) you want to use GitLab,
there will be one SSH key pair. The official GitLab guide describes the
configuration of the GitLab SSH access, but before reading the guide consider
the following explanations:
* Do **only** use existing key pairs if you know that **you** have generated
  them and they were generated for the GitLab access. This prevents security
  issues, because normally every key pair is for an specific application. If you
  are not sure, generate a new one.
* When generating a new key pair, make sure that you **do not override existing
  ones!**
* Use a password (passphrase called by the ssh-keygen command) for the key pair.
* It is recommended to use non-default SSH key pair paths. This will prevent
  conflicts with multiple SSH key pairs (when there are any existing or added
  later). For example create a directory called `keys` in the `~/.ssh` folder
  and save your keys in that directory. Furthermore give your key pair files a
  meaningful name, so that you know for what this key pair is used.
* After generating the key pair, the guide will tell you to add it to GitLab.
  For this open the GitLab homepage and go to your profile settings (click on
  the icon in the top right corner), there you should find the "SSH Keys"
  section. This is where you add your key, but be careful to **NOT ADD THE
  PRIVATE KEY**, only the public key, as described in the guide. Furthermore
  choose a meaningful title for the SSH key which clearly describes for which
  connection it is used (as mentioned above you will probably have multiple
  keys), e.g.: `"gollum - <user name>"`, `"work pc ubuntu - <user name>"`, 
  `"private laptop windows 7 - <user name>"`, ...
* In the section `Working with non-default SSH key pair paths` of the guide,
  ignore the code snippet with the `eval` and `ssh-add` commands, because this
  will be covered by this gude later. **BUT** do not ignore the part just below
  the snippet with the configuration of the `~/.ssh/config` file (`IdentityFile`
  is the path to the private key file, e.g.: `~/.ssh/keys/gitlab`)! 

Now continue with the [official guide from GitLab][gitlab-ssh-guide].  

If you used a passphrase for the private SSH key (which is recommended) than 
everytime a connection needs to be established to the GitLab server, you have to 
enter it (usually when using the git commands `git pull` or `git push`). To 
avoid having to repeatedly do this, you can run an SSH agent. This small utility 
stores your private key after you have entered the passphrase for the first 
time. It will be available for the duration of your terminal session, allowing 
you to connect in the future without re-entering the passphrase. Read the 
[Configure SSH agent][ssh-agent-guide] guide to setup the SSH 
agent.

<!-- links reference -->
[git-download]: https://git-scm.com/downloads
[git-simple-guide]: http://rogerdudler.github.io/git-guide/
[git-basics]: https://git-scm.com/book/en/v2/Getting-Started-Git-Basics
[gitlab-register]: https://gitlab.com/users/sign_in
[jkuisw-group]: https://gitlab.com/jkuisw
[gitlab-ssh-guide]: https://gitlab.com/help/ssh/README.md
[ssh-agent-guide]: /guides/configure-ssh-agent.md
[git-cheatsheet]: https://gitlab.com/gitlab-com/marketing/raw/master/design/print/git-cheatsheet/print-pdf/git-cheatsheet.pdf

Configure SSH agent
===================
First you have to start the SSH agent, the command depends on the type of shell
you are using. 

> **Note:** You can check the type of shell you are running on by executing the
> command `echo $SHELL`.

If you run a bourne shell type shell (e.g. bash) run the command:  
```shell
eval `ssh-agent -s`
```

Or if you are on a csh-type shell (e.g. on gollum) run the command:  

```shell
eval `ssh-agent -c`
```

This will start the agent program and place it into the background. Now, you 
need to add your private key to the agent, so that it can manage your key:

```shell
ssh-add "/path/to/private/key/file"
```

You will have to enter your passphrase. Afterwards, your identity file is added 
to the agent, allowing you to use your key to sign in without having re-enter 
the passphrase again. But this will only work for one terminal session. 
Therefore if you end your terminal session, the next time you have to re-do the 
procedure. To get around this you can write a script which will automatically 
ask at the beginning of the first terminal session started for the passphrase. 
This approach will be covered in the next section.

Auto Start SSH Agent script
---------------------------
Depending on the shell type that you use for login choose the right script 
below, two types are covered by this guide:
* bourne shell type shells (e.g.: bash) 
* csh type shells (e.g.: csh, tcsh)

This is a per user configuration, therefore it only affects the user account to 
which you add the script code (`~` is a shortcut for the home directory of your 
user account, e.g.: `/home/<user name>`). The code below has to be appended to a 
specific file (depending on the shell type). This file will be executed at 
startup of your terminal session (see the man pages of your shell) and will 
basically do the following:
1. Check if the SSH agent is running and if not, starting it.
2. Check if there are already any keys registered (when the agent is already 
  running).
3. If there are already keys registered than it will do nothing.
4. If no keys are registered, than it will register your keys. But the script 
  must know which keys to add and where they are. Therefore you have to 
  configure the script accordingly. To do this, search for the two points in the 
  code below which are marked with the comment `# <ADD YOUR KEYS HERE>`. At whis 
  two points you have to add your keys with the `ssh-add` command.

### Bourne Shell Type Shell
Append the following code to your `~/.bash_profile` file. If the file does not 
exist, create it.
```shell
################################################################################
# Start ssh agent on load and add ssh keys to it.
# Note: ~/.ssh/environment should not be used, as it already has a different 
#   purpose in SSH.
env=~/.ssh/agent.env

# Note: Don't bother checking SSH_AGENT_PID. It's not used by SSH itself, and it 
#   might even be incorrect (for example, when using agent-forwarding over SSH).
agent_is_running() {
  if [ "$SSH_AUTH_SOCK" ]; then
  # ssh-add returns:
  #   0 = agent running, has keys
  #   1 = agent running, no keys
  #   2 = agent not running
  ssh-add -l >| /dev/null 2>&1 || [ $? -eq 1 ]
  else
  	false
  fi
}

agent_has_keys() {
    ssh-add -l >/dev/null 2>&1
}

agent_load_env() {
  source "$env" >| /dev/null ;
}

agent_start() {
  (umask 077; ssh-agent -s >| "$env")
  source "$env" >| /dev/null ;
}

# Load agent environment if the agent is not running.
if ! agent_is_running; then
  agent_load_env
fi

# Add the private keys to the agent.
if ! agent_is_running; then
  agent_start
  # if keys are stored in ~/.ssh/id_rsa or ~/.ssh/id_dsa, only uncomment the 
  # following line.
  #ssh-add 
  
  # if your keys are NOT stored in ~/.ssh/id_rsa or ~/.ssh/id_dsa, add the 
  # private keys with ssh-add and the path to the private key file.
  # e.g.: ssh-add /path/to/private/key
  # <ADD YOUR KEYS HERE>
elif ! agent_has_keys; then
  # if keys are stored in ~/.ssh/id_rsa or ~/.ssh/id_dsa, only uncomment the 
  # following line.
  #ssh-add 
  
  # if your keys are NOT stored in ~/.ssh/id_rsa or ~/.ssh/id_dsa, add the 
  # private keys with ssh-add and the path to the private key file.
  # e.g.: ssh-add /path/to/private/key
  # <ADD YOUR KEYS HERE>
fi

unset env
```

In the code above you have to add your private keys for the agent with the 
`ssh-add` command at two points, which are marked with the comment 
`# <ADD YOUR KEYS HERE>`.

> :warning: When you add no keys, comment the if, because empty if bodies are
> not allowed in bash scripts!

### csh Type Shell
Append the following code to your `~/.login` file. If the file does not exist,
create it.
```shell
###############################################################################
# Start ssh agent on load and add ssh keys to it.
# Note: ~/.ssh/environment should not be used, as it already has a different
#   purpose in SSH.
set env=~/.ssh/agent.env

# Check if agent is running.
set is_not_running = 0

# Note: Don't bother checking SSH_AGENT_PID. It's not used by SSH itself, and
#   it might even be incorrect (for example, when using agent-forwarding over
#   SSH).
if ( $?SSH_AUTH_SOCK ) then
  # ssh-add returns:
  #   0 = agent running, has keys
  #   1 = agent running, no keys
  #   2 = agent not running
  ssh-add -l >& /dev/null
  if ( $? == 2 ) then
    set is_not_running = 1
  endif
else
  set is_not_running = 1
endif

if ( $is_not_running ) then
  # Agent is not running on current shell instance.
  # When an agent was started on another shell instance, then there should be a
  # valid agent environment, therefore try to load it.
  if ( -f $env ) then
    source "$env" > /dev/null
  endif
endif

# Check again if agent is not running.
set is_not_running = 0

# Note: Don't bother checking SSH_AGENT_PID. It's not used by SSH itself, and
#   it might even be incorrect (for example, when using agent-forwarding over
#   SSH).
if ( $?SSH_AUTH_SOCK ) then
  # ssh-add returns:
  #   0 = agent running, has keys
  #   1 = agent running, no keys
  #   2 = agent not running
  ssh-add -l >& /dev/null
  if ( $? == 2 ) then
    set is_not_running = 1
  endif
else
  set is_not_running = 1
endif

if ( $is_not_running ) then
  # Start the agent.
  ssh-agent -c > "$env"
  chmod 600 "$env"
  source "$env" > /dev/null

  # if keys are stored in ~/.ssh/id_rsa or ~/.ssh/id_dsa, only uncomment the
  # following line.
  #ssh-add

  # if your keys are NOT stored in ~/.ssh/id_rsa or ~/.ssh/id_dsa, add the
  # private keys with ssh-add and the path to the private key file.
  # e.g.: ssh-add /path/to/private/key
  # <ADD YOUR KEYS HERE>
else
  # Agent is running. Check if the agent has keys.
  ssh-add -l >& /dev/null
  if ( $? == 1 ) then
    # if keys are stored in ~/.ssh/id_rsa or ~/.ssh/id_dsa, only uncomment the
    # following line.
    #ssh-add

    # if your keys are NOT stored in ~/.ssh/id_rsa or ~/.ssh/id_dsa, add the
    # private keys with ssh-add and the path to the private key file.
    # e.g.: ssh-add /path/to/private/key
    # <ADD YOUR KEYS HERE>
  endif
endif

unset env
```

In the code above you have to add your private keys for the agent with the 
`ssh-add` command at two points, which are marked with the comment 
`# <ADD YOUR KEYS HERE>`.

> :warning: When you add no keys, comment the if, because empty if bodies are
> not allowed in csh/tcsh scripts!

Development Workflow
====================

This workflow is based on the [GitHub flow][github-flow-link]. 

> **Note:** There are other workflows but the [GitHub flow][github-flow-link] is
> a very simple one (though with some disadvantages), which should be sufficient
> for many projects.

Basically you have one production branch, normally the `master` branch, which has
special protection (means only specific users can merge and push to them). When
you implement changes to the repository (new features or bug fixes), than you
create new development branches from the master branch. The development then
takes place on this development branches. When a feature or bug fix for one
development branch is completed (fixed), then you merge that branch back to the
master branch. When all features and bug fixes for a new version are merged back
to the master branch, a new version of the software is created by tagging that
commit. 

The next sections describe that workflow in detail by using [GitLab][gitlab].
Every section is one step of the workflow and each step requires a minimum
permission level.

> **Note:** Each user added as member to a project get's a permission level
> assigned. This permission level decides what a user can do and can NOT do on
> that project, see the
> [GitLab permissions model][gitlab-permissions-model]
> for further details.

> **Note:** The different steps of the workflow could be done by one person, but
> normally there are more people included. For example, a user of a project
> creates an issue, a developer of the project does the planing and
> implementation of that issue and the master of that project releases a new
> version with that issue solved.

1. Create Issues
-------------
`minimum permission level: any`  
The workflow begins with creating issues on [GitLab][gitlab]. For every idea,
new feature or bug found an issue is created and could also lead into multiple
issues. See the [issues documentation][gitlab-issues] on [GitLab][gitlab] for
further information.

2. Plan a Release
------------------
`minimum permission level: Developer`  
The next step is to plan a new release (version) of the project and to decide
which issues will be resolved in that release. Therefore on [GitLab][gitlab]
create a new milestone (preferably named after the new version of the release)
and add the milestone to the issues which should be resolved with that new
version. For further information see the
[milestone documentation][gitlab-milestones] on [GitLab][gitlab].

3. Create Development Branches
---------------------------
`minimum permission level: Developer`  
For the implementation of the issues assigned to the milestone create one or
more development branches from the master branch, following the rules:
* On one branch only one developer is working.
* A branch is merged back to the master branch only through a merge request
  (merge requests will me explained in a later step).
* Each branch resolves one ore more issues, depending on the scope of the
  issues.
* On the branches the development for the new release is done, no development
  on the master branch.
* The branch is deleted after the merge request is resolved (see below).
* Choose a meaningful name for the branch, e.g.:
  + georg-feature-x-for-2.3.4
  + georg-bug-fix-4-for-2.3.4
  + georg-dev-for-2.3.4

Create the branch on [GitLab][gitlab], see the
[branches documentation][gitlab-branches] on [GitLab][gitlab] for further
information.

4. Implementing the Issues
-----------------------
`minimum permission level: Developer`  
Now implement the issues by developing on your created branch(es). To work on a
branch execute the following commands on the computer where you develop.

```shell
# Pull changes from remote repository.
git pull
# Checkout local development branch.
git checkout --track origin/<branch-name>
# Do some changes, than add and commit them. This step is normally done multiple
# times.
git add --all
git commit -m "<commit message>"
# Push the changes to the remote repository.
git push
```

With the first command your local repository will be synchronized with the
remote one on [GitLab][gitlab] (download all changes made on [GitLab][gitlab]).
The next command will than checkout (and create) the local development branch
and add the remote development branch (created with [GitLab][gitlab]) as the
tracking branch. `<branch-name>` must be exactly the name of the remote
development branch, which you have created on [GitLab][gitlab] in the last step.
Than you can start the development. Use the `add` and `commit` commands multiple
times (possibly also the `push` command) to backup and document your
development. Better create more commits than less. After finishing the
implementation of all issues for that branch `add`, `commit` and `push` your
last changes. The next step is to create, review and resolve the merge request,
as explained in the next sections. As a result of the review process you may
have to implemented further changes and again `push` them. When the review
process is finished, the merge request has to be resolved. But unless you have
the `master` permission level (or higher than `master`), the resolving will be
done by a project member with that permission. After the merge request is
resolved, the local development branch and the reference to the remote
development branch can be deleted with the following commands:

```shell
# Go back to master branch.
git checkout master
# Delete local branch.
git branch -d <branch-name>
# Delete reference to remote branch.
git fetch --prune
```

> **Note:** You can list your local branches and references to remote branches
> by executing the command `git branch -a`.

5. Create Merge Request
--------------------
`minimum permission level: Developer`  
When the implementation of a development branch is finished, then the changes
must be merged back to the `master` branch. To do this create a merge request
on [GitLab][gitlab]. See the 
[GitLab merge requests documentation][gitlab-merge-requests] for further
information.

> **Note:** When you have commited changes to the development branch, you can
> create the merge request directly from it. On [GitLab][gitlab] go the the
> branches section of the project and look for your development branch. There
> should be a button called `Merge request`, click it to create the merge
> request.

After creating the merge request, the changes made can be discussed and
reviewed (ideally reviews should not be done by the developer of the changes).
As a result of the discussion and review process, the code may needs to be
changed (e.g.: found a bug, ...) again. This will be done on the same branch and
may also create new issues for that branch and for the milestone. When the
discussion and review process approves the changes, then the merge request can
be resolved, as described in the next step (section).

6. Resolve Merge Request
---------------------
`minimum permission level: Master`  
After a successful discussion and review process the merge request can be
resolved. When you accept (resolve) a merge request, make sure that the
`remove source branch` check-box is marked. After accepting the merge request,
the changes will be merged into the master branch and the development branch
will be deleted (when the `remove source branch` check-box was marked). Normally
the master branch is protected and at least the `master` permission level is
needed to accept a merge request. Therefore normally not everyone can do this
step.

7. Create New Version
------------------
`minimum permission level: Master`  
When all issues and merge requests of the milestone are resolved, then a new
version can be created. To do so, on [GitLab][gitlab], go to the `Tags` page
(in the `Repository` menu) and click the button `New tag`. As the tag name enter
the new version and fill out the `Release notes`. The `Release notes` should
describe at least:
* Breaking changes
* Added features
* Fixed bugs
* Deprecated features
* Internal changes = changes that do not affect the public API of the software

After successfully creating the new version, the milestone created in step 2 can
be closed.



<!-- links reference -->
[github-flow-link]: https://docs.gitlab.com/ee/workflow/gitlab_flow.html#github-flow-as-a-simpler-alternative
[gitlab]: https://gitlab.com/
[gitlab-permissions-model]: https://docs.gitlab.com/ee/user/permissions.html
[gitlab-issues]: https://docs.gitlab.com/ee/user/project/issues/index.html
[gitlab-milestones]: https://docs.gitlab.com/ee/user/project/milestones/index.html
[gitlab-branches]: https://docs.gitlab.com/ee/user/project/repository/branches/index.html
[gitlab-merge-requests]: https://docs.gitlab.com/ee/user/project/merge_requests/index.html

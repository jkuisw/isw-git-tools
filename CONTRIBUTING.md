Contributing to ISW Git Tools
=============================

It is highly appreciated if you want to contribute, but please read the
following guide carefully before you do so.

Directory Structure
-------------------
The following code snippet shows the directory structure of the repository.

```
isw-git-tools
|--+ guides
|--- repo-scripts
|  |--- fluent-scheme
|  |  |--+ scripts
|  |  |--+ templates
|  |  |--- CONTRIBUTING.md
|  |  +--- README.md
|  |
|  |--- tcl-tk
|  |  |--+ scripts
|  |  |--+ templates
|  |  |--- CONTRIBUTING.md
|  |  +--- README.md
|  |
|  +--- <support for any language>
|     |--+ scripts
|     |--+ templates
|     |--- CONTRIBUTING.md
|     +--- README.md
|
|--+ scripts
|--- .gitignore
|--- CONTRIBUTING.md
+--- README.md
```

### The `guides` directory
Is the location for guides that describe general tasks. Currently the following
guides exist:
* Setup [GitLab][gitlab] access - Describes the process for configuring the SSH
  access to [GitLab][gitlab].
* Configure SSH agent - Describes the process for configuring the SSH agent and
  provides a script for automating the agent start-up and registering of private
  keys to the agent.
* Development workflow - Describes a possible workflow to develop a repository.

### The `scripts` directory
Is the location for scripts that do general tasks which can be used by different
other scripts. The following subsections describe the currently implemented
general scripts.

#### `git` scripts
The `git` sub-folder contains the following scripts:

##### `gitCheckForChanges.sh`
Checks if in the current repository are any changes were made. The current
repository is where it is executed, or if a directory is passed as the first
parameter, than the repository where that directory is located. See the source
code comments for further information.

##### `gitIsCurrentBranch.sh`
Checks if the branch passed is the current (checked out) branch of the
repository. See the source code comments for further information.

##### `gitNewVersionTag.sh`
Creates a new git version tag by processing the following steps:
1. Add all changes to the staging area (`git add --all`).
2. Commit all changes (`git commit -m "<commit message>"`).
3. Create the new tag (`git tag "<the new version>" -m "<commit message>"`).
4. Push all changes to the remote repository (`git push --all && git push --tags`).

See the source code comments for further information.

#### `semver` scripts
Currently this directory contains only one script, the `semver.sh` script, which
provides a function to check if a version follows the 
[semantic versioning specification][semver-spec] and a function to compare two
semantic versions. See the source code comments for further information.

### The `repo-scripts` directory
This directory contains other sub-directories, each of them supports a different
language, from now on called *"language support"*. A *language support* must at
least contain a `README.md` file (describes the usage of the language support)
and a `CONTRIBUTING.md` file (describes the implementation of it and what one
must know to join development of it). All other files and/or directories are
optional and depend on the type of the language, but in most cases a `scripts`
directory (which contains the implementations for the support) and a `templates`
directory (which contains templates to copy) are recommendable. 

Add a new Language Support
------------------------
To add a new language support, create a directory in the `repo-scripts`
directory and choose a meaningful name for the directory (the folder name is the
name of the language support, please avoid spaces in the name).

```shell
cd "/path/to/repo-scripts"
mkdir "<name of language support>"
```

In that newly created directory create at least a `README.md` file and a
`CONTRIBUTING.md` file. 

```shell
cd "/path/to/repo-scripts/<name of language support>"
touch README.md
touch CONTRIBUTING.md
```

The `README.md` file should describe what the language support offers, how to
use it and how to configure. The `CONTRIBUTING.md` file should describe the
implementation of the language support and what one must know to join the
development of it.

Think what all repositories of that new language could need and have in common.
In most cases you will at least need a script for creating versions of the
repository, but you may also need a script that generates API documentation or
a packaging script. This can not be answered generally, it depends on the type
of the language. Look through the existing language supports what they have
implemented and how. Probably you can copy and adjust code from them for the new
language.

Development Workflow
--------------------
The production branch for the `isw-git-tools` repository is the `master` branch,
which is also a [protected branch][gitlab-protected-branches]. Only users with
at least the `Master` permission level can push or merge to this branch (see the
[GitLab permissions model][gitlab-permissions-model] for further information).
For the development of this repository the [Development Workflow][dev-workflow]
should be used. Please read this guide and try to follow it.

Versioning
----------
You may only create versions of this repository that follow the
[semantic versioning specification][semver-spec]. To create a new version, just
go to the project page on [GitLab][gitlab]. Search for the `Tags` submenu in the
`Repository` menu. Click the `New tag` button and choose for the tag name a
version following the [semantic versioning specification][semver-spec].
Furthermore document in the release notes what has changed to the prior version
(following the [Development Workflow][dev-workflow]). Then click the `Create tag`
button.

Language Support Contributing Guides
------------------------------------
Each language support does have it's own contributing guide which extends this
one with language specific explanations. Please read this guides before
developing on a specific language support.



<!-- links reference -->
[semver-spec]: http://semver.org/
[dev-workflow]: /guides/dev-workflow.md
[gitlab]: https://gitlab.com/
[gitlab-permissions-model]: https://docs.gitlab.com/ee/user/permissions.html
[gitlab-protected-branches]: https://docs.gitlab.com/ee/user/project/protected_branches.html#protected-branches
